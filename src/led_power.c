/*  XMMS - Cross-platform multimedia player
 *  Copyright (C) 1998-2000  Peter Alm, Mikael Alm, Olle Hallnas, Thomas Nilsson and 4Front Technologies
 *
 *  Led Power - Visualization Plugin for XMMS
 *  Copyright (C) 2004 mock
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  mock
 *  led_power.c
 */

#include "led_power.h"



static Configuration cfg;
static Scheme *schemes;
static int schemes_count = 0;
static int dev_count = 0;


static void vis_init (void);
static void vis_start (void);
static void vis_stop (void);
static void vis_render_pcm (gint16 data[2][512]);
static void vis_cleanup (void);

static void vis_set_leds (void);


VisPlugin vp =
{
#ifndef USING_BMP            // vp for XMMS
	NULL,                /* handle (xmms)            */
	NULL,                /* filename (xmms)          */
	0,                   /* xmms session ID          */
	"Led Power",         /* description              */
	2,                   /* num_pcm in render_pcm    */
	0,                   /* num_freq in render_freq  */
	vis_init,            /* init (void)              */
	vis_cleanup,         /* cleanup (void)           */
#ifndef NOCONFIG
	config_about,        /* about (void)             */
	config_dialog,       /* configure (void)         */
#else
	NULL,                /* about (void)             */
	NULL,                /* configure (void)         */
#endif
	NULL,                /* disable_plug (VisPlugin) */
	vis_start,           /* playback_start (void)    */
	vis_stop,            /* playback_stop (void)     */
	vis_render_pcm,      /* render_pcm (gint16 pcm_data[2][512])   */
	NULL                 /* render_freq (gint16 freq_data[2][256]) */
#else                        // vp for Audacious
	NULL,                /* handle (xmms)            */
	NULL,                /* filename (xmms)          */
	"Led Power",         /* description              */
	vis_init,            /* init (void)              */
	vis_cleanup,         /* cleanup (void)           */
#ifndef NOCONFIG
	config_about,        /* about (void)             */
	config_dialog,       /* configure (void)         */
#else
	NULL,                /* about (void)             */
	NULL,                /* configure (void)         */
#endif
	0,                   /* enabled                  */

	2,                   /* num_pcm in render_pcm    */
	0,                   /* num_freq in render_freq  */

	NULL,                /* disable_plug (VisPlugin) */
	vis_start,           /* playback_start (void)    */
	vis_stop,            /* playback_stop (void)     */
	vis_render_pcm,      /* render_pcm (gint16 pcm_data[2][512])   */
	NULL                 /* render_freq (gint16 freq_data[2][256]) */
#endif
};


LedDevice devices[] =
{
#ifndef NOSCREENLEDS
	{"screen_leds", SCR_LEDS_COUNT,
	 screen_open, screen_close, screen_refresh,
	 screen_led_on, screen_led_off,
	 screen_leds_all_on, screen_leds_all_off,
	 screen_leds_invert, screen_configure,
	 0, 0},
#endif
#ifndef NOLPTLEDS
	{"lpt_leds", LPT_LEDS_COUNT,
	 lpt_open, lpt_close, lpt_refresh,
	 lpt_led_on, lpt_led_off,
	 lpt_leds_all_on, lpt_leds_all_off,
	 lpt_leds_invert, lpt_configure,
	 0, 0},
#endif
#ifndef NOKEYBOARDLEDS
	{"keyboard_leds", KBD_LEDS_COUNT,
	 keyboard_open, keyboard_close, keyboard_refresh,
	 keyboard_led_on, keyboard_led_off,
	 keyboard_leds_all_on, keyboard_leds_all_off,
	 keyboard_leds_invert, keyboard_configure,
	 0, 0},
#endif
#ifndef NOFLOPPYLEDS
	{"floppy_leds", FLOPPY_LEDS_COUNT,
	 floppy_open, floppy_close, floppy_refresh,
	 floppy_led_on, floppy_led_off,
	 floppy_leds_all_on, floppy_leds_all_off,
	 floppy_leds_invert, floppy_configure,
	 0, 0},
#endif
};

int *scheme_num[] =
{
#ifndef NOSCREENLEDS
	&cfg.screen_leds_scheme,
#endif
#ifndef NOLPTLEDS
	&cfg.lpt_leds_scheme,
#endif
#ifndef NOKEYBOARDLEDS
	&cfg.keyboard_leds_scheme,
#endif
#ifndef NOFLOPPYLEDS
	&cfg.floppy_leds_scheme,
#endif
};

#define DEVICE_SCHEME_NUM(d) (*(scheme_num[d]))

#ifndef USING_BMP
VisPlugin *get_vplugin_info (void)
{
	config_read (&cfg);

	return &vp;
}
#else
VisPlugin *vp_vplist[] = { &vp, NULL };
DECLARE_PLUGIN (vis, NULL, NULL, NULL, NULL, NULL, NULL, vp_vplist, NULL);
#endif


static void vis_init (void)
{
	int i;


#ifdef USING_BMP
	config_read (&cfg);
#endif

	dev_count = ARRAY_SIZE (devices);
	sp_init (&cfg);
	schemes_count = sp_get_schemes (&schemes);

	for (i = 0; i < dev_count; i++)
	{
		if (DEVICE_SCHEME_NUM (i) < 0 || DEVICE_SCHEME_NUM (i) > schemes_count)
			DEVICE_SCHEME_NUM (i) = 0;

		devices[i].configure (&cfg);
	}

#ifndef NOCONFIG
	config_update ();
#endif

#ifndef NOIDLE
	idle_enter_after_time (TIME_BEFORE_CLEANUP, TIME_BEFORE_IDLE);
#endif
}


static void vis_start (void)
{
#ifndef NOIDLE
	idle_exit ();

	{
		int i;

		for (i = 0; i < dev_count; i++)
		{
			devices[i].leds_all_off ();
			devices[i].refresh ();
		}
	}
#endif
}


static void vis_stop (void)
{
#ifndef NOIDLE
	idle_enter_after_time (TIME_BEFORE_CLEANUP, TIME_BEFORE_IDLE);
#else
	{
		int i;

		for (i = 0; i < dev_count; i++)
		{
			devices[i].leds_all_off ();
			devices[i].refresh ();
		}
	}
#endif
	sp_reset ();
}


int vis_song_changed (void)
{
	static int last_time = 0;
	int pos, time;

/*
	pos = xmms_remote_get_playlist_pos (vp.xmms_session);
	time = xmms_remote_get_playlist_time (vp.xmms_session, pos);
*/
	pos = audacious_remote_get_playlist_pos (0);
	time = audacious_remote_get_playlist_time (0, pos);

	if (last_time != time)
	{
		last_time = time;

		return TRUE;
	}

	return FALSE;
}


static void vis_render_pcm (gint16 data[2][512])
{
	if (vis_song_changed ())
		sp_reset ();

	sp_process_pcm_data (data, cfg.general_filter);

	vis_set_leds ();
}


static void vis_cleanup (void)
{
	int i;


	for (i = 0; i < dev_count; i++)
		devices[i].close ();

	sp_cleanup ();

#ifndef NOIDLE
	idle_exit ();
#endif
}


static void vis_set_leds (void)
{
	int i;


	for (i = 0; i < dev_count; i++)
	{
		schemes[DEVICE_SCHEME_NUM (i)].process (&devices[i]);
		devices[i].refresh ();
	}
}


void vis_disable (void)
{
	vp.disable_plugin (&vp);
}


void vis_configuration_changed (void)
{
#ifndef NOCONFIG
	config_update ();
#endif
}


Configuration *vis_get_configuration (void)
{
	return &cfg;
}


int vis_get_devices (LedDevice **p)
{
	*p = devices;

	return dev_count;
}


int vis_get_song_time (void)
{
	int i;


/*
	i = xmms_remote_get_playlist_pos (vp.xmms_session);
	return xmms_remote_get_playlist_time (vp.xmms_session, i);
*/
	i = audacious_remote_get_playlist_pos (0);
	return audacious_remote_get_playlist_time (0, i);
}


int vis_get_output_time (void)
{
//	return xmms_remote_get_output_time (vp.xmms_session);
	return audacious_remote_get_output_time (0);
}


float vis_get_volume (void)
{
//	return (float) xmms_remote_get_main_volume (vp.xmms_session) / 100;
	return (float) audacious_remote_get_main_volume (0) / 100;
}


int cfg_read_boolean (ConfigFile *cf, char *section, char *key, int default_val)
{
	int val = default_val;
	
#ifndef USING_BMP
	xmms_cfg_read_boolean (cf, section, key, &val);
#else
//	cfg_db_get_bool (cf, section, key, &val);
#endif
	return val;
}


int cfg_read_int (ConfigFile *cf, char *section, char *key, int default_val)
{
	int val = default_val;
	
#ifndef USING_BMP
	xmms_cfg_read_int (cf, section, key, &val);
#else
//	cfg_db_get_int (cf, section, key, &val);
#endif
	return val;
}


char *cfg_read_string (ConfigFile *cf, char *section, char *key, char *default_str)
{
	char *str;
	
#ifndef USING_BMP
	if (xmms_cfg_read_string (cf, section, key, &str))
#else
//	if (cfg_db_get_string (cf, section, key, &str))
	if (0)
#endif
		return str;
	else if (default_str)
		return strdup (default_str);
	else
		return NULL;
}


void cfg_write_boolean (ConfigFile *cf, char *section, char *key, int val)
{
#ifndef USING_BMP
	xmms_cfg_write_boolean (cf, section, key, val);
#else
//	cfg_db_set_bool (cf, section, key, val);
#endif
}


void cfg_write_int (ConfigFile *cf, char *section, char *key, int val)
{
#ifndef USING_BMP
	xmms_cfg_write_int (cf, section, key, val);
#else
//	cfg_db_set_int (cf, section, key, val);
#endif
}


void cfg_write_string (ConfigFile *cf, char *section, char *key, char *str)
{
#ifndef USING_BMP
	xmms_cfg_write_string (cf, section, key, str);
#else
	//cfg_db_set_string (cf, section, key, str);
#endif
}


ConfigFile *cfg_open_default_file (void)
{
#ifndef USING_BMP
	return xmms_cfg_open_default_file ();
#else
//	return cfg_db_open ();
	return 1; // so that default key values will be used
#endif
}


void cfg_close (ConfigFile *cf)
{
#ifndef USING_BMP
	xmms_cfg_free (cf);
#else
//	cfg_db_close (cf);
#endif
}


void cfg_write (ConfigFile *cf)
{
#ifndef USING_BMP
	xmms_cfg_write_default_file (cf);
#endif
}
