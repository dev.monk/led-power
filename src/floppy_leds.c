/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  floppy_leds.c
 */

#include "floppy_leds.h"
#include "led_power.h"


static int floppy_leds[] =
{
	LED_01,
#if FLOPPY_LEDS_COUNT == 2
	LED_02,
#endif
};

static int leds = 0;
static int opened = FALSE;
static int saved_state = 0;
static int configured = FALSE;

static CfgFloppy cfg;


int floppy_open (void)
{
	if (opened)
		return TRUE;

	if (!configured)
		floppy_configure (NULL);

	// 'ioperm' returns 0 on success and -1 on error
	opened = ioperm (PORT, 3, TRUE) + 1;

	if (!opened)
	{
		perror ("FLOPPY_LEDS!: cannot open port");

		return FALSE;
	}

	if (FLOPPY_REMEMBER_VALUE)
		saved_state = inb (PORT);

	floppy_leds_all_off ();
	floppy_refresh ();

	return TRUE;
}


void floppy_close (void)
{
	if (!opened)
		return;

	if (FLOPPY_REMEMBER_VALUE)
		outb (saved_state, PORT);
	ioperm (PORT, 3, FALSE);

	opened = FALSE;
}


void floppy_refresh (void)
{
	static int last_leds_state = 0;


	if (last_leds_state == leds)
		return;
	else
		last_leds_state = leds;

	if (opened)
	{
		if (leds & LED_01)
			outb (DRIVE_A_LED_ON, PORT);
		else
			outb (DRIVE_A_LED_OFF, PORT);

		if (FLOPPY_LEDS_COUNT == 2)
		{
			if (leds & LED_02)
				outb (DRIVE_B_LED_ON, PORT);
			else
				outb (DRIVE_B_LED_OFF, PORT);
		}
	}
}


void floppy_led_on (unsigned int led)
{
	int l;


	if (led >= FLOPPY_LEDS_COUNT)
		return;
//
//	while (l < FLOPPY_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	if (cfg.leds_invert)
		leds &= ~floppy_leds[l];
	else
		leds |= floppy_leds[l];
}


void floppy_led_off (unsigned int led)
{
	int l;


	if (led >= FLOPPY_LEDS_COUNT)
		return;
//
//	while (l < FLOPPY_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	if (cfg.leds_invert)
		leds |= floppy_leds[l];
	else
		leds &= ~floppy_leds[l];
}


void floppy_leds_all_on (void)
{
	if (cfg.leds_invert)
		leds = FLOPPY_LED_NONE;
	else
		leds = FLOPPY_LED_ALL;
}


void floppy_leds_all_off (void)
{
	if (cfg.leds_invert)
		leds = FLOPPY_LED_ALL;
	else
		leds = FLOPPY_LED_NONE;
}


void floppy_leds_invert (void)
{
	cfg.leds_invert = !cfg.leds_invert;
}


void floppy_leds_order_invert (void)
{
	int i, j;


	for (i = 0; i < FLOPPY_LEDS_COUNT / 2; i++)
	{
		j = cfg.leds_order[i];
		cfg.leds_order[i] = cfg.leds_order[FLOPPY_LEDS_COUNT - 1 - i];
		cfg.leds_order[FLOPPY_LEDS_COUNT - 1 - i] = j;
	}
}


void floppy_configure (Configuration * p)
{
	int i;


	configured = TRUE;

	if (p == NULL)
	{
		// setting defaults

		cfg.leds_on = TRUE;
		cfg.leds_invert = FALSE;

		for (i = 0; i < FLOPPY_LEDS_COUNT; i++)
			cfg.leds_order[i] = i;
		cfg.leds_order[FLOPPY_LEDS_COUNT] = -1;

		cfg.leds_order_invert = FALSE;
	}
	else
	{
		cfg.leds_invert = p->floppy_leds_invert;
		cfg.leds_order_invert = p->floppy_leds_order_invert;

		for (i = 0; i < FLOPPY_LEDS_COUNT; i++)
			cfg.leds_order[i] = p->floppy_leds_order[i];

		if (cfg.leds_order_invert)
			floppy_leds_order_invert ();

		if (p->floppy_leds_on)
		{
			cfg.leds_on = floppy_open ();
		}
		else
		{
			floppy_close ();
			cfg.leds_on = FALSE;
		}

                p->floppy_leds_on = cfg.leds_on;
	}
}
