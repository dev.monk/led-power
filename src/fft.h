#ifndef __FFT_H
#define __FFT_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <math.h>
#include <stdlib.h>


void fft_prepare (int Nlog2);
void fft (float *outr, short *in);
void fft_cleanup (void);


#endif // __FFT_H
