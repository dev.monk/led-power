/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  fft.c
 */

#include "fft.h"


static int gNlog2, gN;
static int *brev_table = NULL;
static float *cos_table = NULL, *sin_table = NULL;


// bit reversal
static int brev (int a, int bits)
{
	int i, b = 0, ta = 1, tb = 1 << (bits - 1);


	for (i = 0; i < bits; i++, ta <<= 1, tb >>= 1)
		if (a & ta)
			b |= tb;

	return b;
}


void fft_cleanup (void)
{
	if (cos_table)
	{
		free (cos_table);
		cos_table = NULL;
	}

	if (brev_table)
	{
		free (brev_table);
		brev_table = NULL;
	}
}


void fft_prepare (int Nlog2)
{
	int i;
	float w;


	if (brev_table)
		fft_cleanup ();

	gNlog2 = Nlog2;
	gN = 1 << gNlog2;

	brev_table = malloc (gN * sizeof (int));
	cos_table = malloc (gN * 0.75 * sizeof (float));

	// prepare bit reversal table
	for (i = 0; i < gN; i++)
		brev_table[i] = brev (i, gNlog2);

	// prepare sin/cos tables
	w = -2 * M_PI / gN;
	for (i = 0; i < gN * 0.75; i++)
		cos_table[i] = cos (w * i);

	sin_table = &cos_table[gN / 4];
}


// fft for real series (imaginary part is equal zero)
// the results will be symmetric about the Nyquist frequency
void fft (float *outr, short *in)
{
	int i, j, k, m, mh, le, lo, t, tt;
	float wr, wi, vr, vi;
	float *outi;


	outi = outr + 1;

	m = 2;
	mh = 1;
	tt = gN / 2;

	for (i = 0; i < gN; i++)
	{
		outr[2 * i] = in[brev_table[i]];
		outi[2 * i] = 0;
	}

	for (i = 1; i <= gNlog2; i++)
	{
		for (k = 0; k < mh; k++)
		{
			// w = exp (-2*pi*(i)*k/m);
			t = k * tt;
			wr = cos_table[t];
			wi = sin_table[t];

			for (j = k; j < gN; j += m)
			{
				le = 2 * j;
				lo = le + m;

				// u = out(even);
				// v = out(odd) * w;
				vr = outr[lo] * wr - outi[lo] * wi;
				vi = outi[lo] * wr + outr[lo] * wi;

				// out(odd) = u - v;
				outr[lo] = outr[le] - vr;
				outi[lo] = outi[le] - vi;
				// out(even) = u + v;
				outr[le] += vr;
				outi[le] += vi;
			}
		}
		// m = 2^i;
		// mh = m/2;
		m <<= 1;
		mh <<= 1;
		tt >>= 1;
	}
}
