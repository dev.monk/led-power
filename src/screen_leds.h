#ifndef __SCREEN_LEDS_H
#define __SCREEN_LEDS_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "configuration.h"

// 1 <= SCR_LEDS_COUNT <= MAX_LEDS_COUNT
#define SCR_LEDS_COUNT			0x08
#define LED_BG_COLOR				0x00000000
#define LED_ON_COLOR_START	0x00ff0000
#define LED_ON_COLOR_MID		0x00ff0000
#define LED_ON_COLOR_END		0x00ff0000
#define LED_OFF_COLOR_START	0x00440000
#define LED_OFF_COLOR_MID		0x00440000
#define LED_OFF_COLOR_END		0x00440000

// WIN_WIDTH (WIN_HEIGHT if HORIZONTAL is set) must be greater than
// (SCR_LEDS_COUNT - 1) * LED_PADDING + 2 * WIN_PADDING
#define WIN_WIDTH			190
#define WIN_HEIGHT		20
#define LED_PADDING		2
#define WIN_PADDING		8
#define HORIZONTAL		1

#define LED_BG_COLOR_NUM		0x00
#define LED_BASE_COLOR_NUM	0x01
#define WIN_TITLE						"Screen Leds"


typedef struct CfgScreenStruct
{
	int leds_on;
	int leds_invert;
	int leds_order[SCR_LEDS_COUNT];
	int leds_order_invert;
} CfgScreen;


int screen_open (void);
void screen_close (void);
void screen_refresh (void);
void screen_led_on (unsigned int led);
void screen_led_off (unsigned int led);
void screen_leds_all_on (void);
void screen_leds_all_off (void);
void screen_leds_invert (void);
void screen_leds_order_invert (void);
void screen_configure (Configuration *p);


#endif // __SCREEN_LEDS_H
