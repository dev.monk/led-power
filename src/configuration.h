#ifndef __CONFIGURATION_H
#define __CONFIGURATION_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define SECTION_NAME		"Led_Power"
#define MAX_LEDS_COUNT		0x10


typedef struct ConfigurationStruct
{
#ifndef NOSCREENLEDS
	int screen_leds_on;
	int screen_leds_invert;
	int screen_leds_order[MAX_LEDS_COUNT + 1];
	int screen_leds_order_invert;
	int screen_leds_scheme;
#endif

#ifndef NOLPTLEDS
	int lpt_leds_on;
	int lpt_port_address;
	int lpt_leds_invert;
	int lpt_leds_order[MAX_LEDS_COUNT + 1];
	int lpt_leds_order_invert;
	int lpt_leds_scheme;
#endif

#ifndef NOKEYBOARDLEDS
	int keyboard_leds_on;
	int keyboard_leds_invert;
	int keyboard_leds_order[MAX_LEDS_COUNT + 1];
	int keyboard_leds_order_invert;
	int keyboard_leds_scheme;
#endif

#ifndef NOFLOPPYLEDS
	int floppy_leds_on;
	int floppy_leds_invert;
	int floppy_leds_order[MAX_LEDS_COUNT + 1];
	int floppy_leds_order_invert;
	int floppy_leds_scheme;
#endif

	int general_filter;
	int general_volume_dynamic;
	int general_volume_max;
	int general_range_low;
	int general_range_high;
} Configuration;


void config_read (Configuration *cfg);
void config_write (Configuration *cfg);


#endif // __CONFIGURATION_H
