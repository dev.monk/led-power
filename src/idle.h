#ifndef __IDLE_H
#define __IDLE_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <glib.h>
#include <math.h>
#include <time.h>


#include "sound_processing.h"
#include "configuration.h"


#define TIMEOUT_INTERVAL	(1500/MAX_LEDS_COUNT)
#define FLASH_DELAY				(800/TIMEOUT_INTERVAL)
#define FADE_TRICK_LOOP		800


typedef struct IdleCleanupSchemeStruct
{
	char *name;
	int (*process) (LedDevice *dev);
} IdleCleanupScheme;

typedef struct IdleSchemeStruct
{
	char *name;
	void (*process) (LedDevice *dev);
} IdleScheme;


void idle_enter (void);
void idle_enter_after_time (unsigned int time_before_cleanup, unsigned int time_before_idle);
void idle_exit (void);


#endif // __IDLE_H
