
#include "config_dialog.h"
#include "led_power.h"
#include "led_power_logo.xpm"


static int setting_defaults = FALSE;
static Configuration *cfg;

static GtkWidget *window_config;
static GtkWidget *notebook;
static int notebook_page = 0;
static GdkPixmap *pixmap = NULL, *mask = NULL;

#ifndef NOSCREENLEDS
static GtkWidget *check_screen_leds_on;
static GtkWidget *check_screen_leds_inverted;
static GtkWidget *check_screen_order_inverted = NULL;
static GtkWidget *combo_screen_scheme;
static GtkWidget *hbox_screen_order_leds;
#endif

#ifndef NOLPTLEDS
static GtkWidget *check_lpt_leds_on;
static GtkWidget *check_lpt_leds_inverted;
static GtkWidget *check_lpt_order_inverted;
static GtkWidget *combo_lpt_scheme;
static GtkWidget *combo_lpt_port;
static GtkWidget *hbox_lpt_order_leds;
#endif

#ifndef NOKEYBOARDLEDS
static GtkWidget *check_keyboard_leds_on;
static GtkWidget *check_keyboard_leds_inverted;
static GtkWidget *check_keyboard_order_inverted;
static GtkWidget *combo_keyboard_scheme;
static GtkWidget *hbox_keyboard_order_leds;
#endif

#ifndef NOFLOPPYLEDS
static GtkWidget *check_floppy_leds_on;
static GtkWidget *check_floppy_leds_inverted;
static GtkWidget *check_floppy_order_inverted = NULL;
static GtkWidget *combo_floppy_scheme;
static GtkWidget *hbox_floppy_order_leds;
#endif

static GtkWidget *combo_general_filter;
static GtkObject *adjustment_general_volume_max;
static GtkObject *adjustment_general_range_low;
static GtkObject *adjustment_general_range_high;
static GtkWidget *check_general_volume_dynamic;
static GtkWidget *hscale_general_volume_max;


static void create_window_config (void);
static void on_window_config_destroy (GtkWidget * widget, gpointer data);
static void on_button_close_clicked (GtkButton * button, gpointer data);
static void config_dialog_set_defaults (Configuration * cfg);

#ifndef NOSCREENLEDS
static void on_check_screen_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_check_screen_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_combo_screen_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data);
static void on_toggle_screen_led_toggled (GtkToggleButton * togglebutton, gpointer data);
static void show_screen_leds_order (int);
#endif

#ifndef NOLPTLEDS
static void on_check_lpt_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_check_lpt_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer);
static void on_check_lpt_order_inverted_toggled (GtkToggleButton * togglebutton, gpointer);
static void on_combo_lpt_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data);
static void on_combo_lpt_port_list_select_child (GtkList * list, GtkWidget * widget, gpointer data);
static void on_toggle_lpt_led_toggled (GtkToggleButton * togglebutton, gpointer data);
static void show_lpt_leds_order (int);
#endif

#ifndef NOKEYBOARDLEDS
static void on_check_keyboard_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_check_keyboard_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_check_keyboard_order_inverted_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_combo_keyboard_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data);
static void on_toggle_keyboard_led_toggled (GtkToggleButton * togglebutton, gpointer data);
static void show_keyboard_leds_order (int);
#endif

#ifndef NOFLOPPYLEDS
static void on_check_floppy_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_check_floppy_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_check_floppy_order_inverted_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_combo_floppy_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data);
static void on_toggle_floppy_led_toggled (GtkToggleButton * togglebutton, gpointer data);
static void show_floppy_leds_order (int);
#endif

static void on_combo_general_filter_list_select_child (GtkList * list, GtkWidget * widget, gpointer data);
static void on_check_general_volume_dynamic_toggled (GtkToggleButton * togglebutton, gpointer data);
static void on_adjustment_general_volume_max_value_changed (GtkAdjustment * adjustment, gpointer data);
static void on_adjustment_general_range_low_value_changed (GtkAdjustment * adjustment, gpointer data);
static void on_adjustment_general_range_high_value_changed (GtkAdjustment * adjustment, gpointer data);


static void on_window_config_destroy (GtkWidget * widget, gpointer data)
{
	window_config = NULL;
}


static void on_button_close_clicked (GtkButton * button, gpointer data)
{
	gtk_widget_destroy (window_config);

	config_write (cfg);
}


#ifndef NOSCREENLEDS
static void on_check_screen_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->screen_leds_on = !cfg->screen_leds_on;
		screen_configure (cfg);
	}
}


static void on_check_screen_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->screen_leds_invert = !cfg->screen_leds_invert;
		screen_configure (cfg);
	}
}


static void on_check_screen_order_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->screen_leds_order_invert = !cfg->screen_leds_order_invert;
		screen_configure (cfg);
	}
}


static void on_combo_screen_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data)
{
	cfg->screen_leds_scheme = gtk_list_child_position (GTK_LIST (list), widget);
}


static void on_toggle_screen_led_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	static GtkToggleButton *last_togglebutton = NULL;
	static gpointer last_data = NULL;
	static int change_focus = FALSE;
	int i, j;


	if (change_focus)
		return;

	if (!gtk_toggle_button_get_active (togglebutton))
	{
		last_togglebutton = NULL;
		last_data = NULL;
	}
	else if (last_togglebutton)
	{
		change_focus = TRUE;
		gtk_toggle_button_set_active (last_togglebutton, FALSE);
		gtk_toggle_button_set_active (togglebutton, FALSE);
		change_focus = FALSE;

		last_togglebutton = NULL;

		i = (int) last_data;
		j = (int) data;

		SWAP (cfg->screen_leds_order[i], cfg->screen_leds_order[j], int) show_screen_leds_order (FALSE);

		screen_configure (cfg);
	}
	else
	{
		last_togglebutton = togglebutton;
		last_data = data;
	}
}


static void show_screen_leds_order (int reset)
{
	static GtkWidget *toggle_screen_led[MAX_LEDS_COUNT];
	char screen_led_names[MAX_LEDS_COUNT][4];
	int i, leds_count;
	LedDevice *devices;


	vis_get_devices (&devices);
	leds_count = devices[SCREENLEDS_N].leds_count;
	if (leds_count > 16)
		return;

	memset (screen_led_names, 0, sizeof (screen_led_names));

	for (i = 0; i < leds_count; i++)
		sprintf (screen_led_names[i], "%d", i + 1);

	if (reset)
		toggle_screen_led[0] = NULL;

	if (toggle_screen_led[0])
	{
		for (i = 0; i < leds_count; i++)
		{
			gtk_widget_destroy (toggle_screen_led[i]);
			toggle_screen_led[i] = NULL;
		}
	}

	for (i = 0; i < leds_count; i++)
	{
		toggle_screen_led[i] = gtk_toggle_button_new_with_label (screen_led_names[cfg->screen_leds_order[i]]);
		gtk_widget_ref (toggle_screen_led[i]);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "toggle_screen_led", toggle_screen_led[i], (GtkDestroyNotify) gtk_widget_unref);
		gtk_box_pack_start (GTK_BOX (hbox_screen_order_leds), toggle_screen_led[i], FALSE, TRUE, 0);
		GTK_WIDGET_UNSET_FLAGS (toggle_screen_led[i], GTK_CAN_FOCUS);

		gtk_signal_connect (GTK_OBJECT (toggle_screen_led[i]), "toggled", GTK_SIGNAL_FUNC (on_toggle_screen_led_toggled), (gpointer) i);
	}

	for (i = 0; i < leds_count; i++)
		gtk_widget_show (toggle_screen_led[i]);
}
#endif // NOSCREENLEDS


#ifndef NOLPTLEDS
static void on_check_lpt_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->lpt_leds_on = !cfg->lpt_leds_on;
		lpt_configure (cfg);
                if (!cfg->lpt_leds_on)
                    togglebutton->active = FALSE;
	}
}


static void on_check_lpt_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->lpt_leds_invert = !cfg->lpt_leds_invert;
		lpt_configure (cfg);
	}
}


static void on_check_lpt_order_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->lpt_leds_order_invert = !cfg->lpt_leds_order_invert;
		lpt_configure (cfg);
	}
}


static void on_combo_lpt_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data)
{
	cfg->lpt_leds_scheme = gtk_list_child_position (GTK_LIST (list), widget);
}


static void on_combo_lpt_port_list_select_child (GtkList * list, GtkWidget * widget, gpointer data)
{
	if (!setting_defaults)
	{
		static int addresses[] = {
			0x0278, 0x0378, 0x02BC, 0x03BC
		};
		int i;


		i = gtk_list_child_position (GTK_LIST (list), widget);
		cfg->lpt_port_address = addresses[i];
		lpt_configure (cfg);
	}
}


static void on_toggle_lpt_led_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	static GtkToggleButton *last_togglebutton = NULL;
	static gpointer last_data = NULL;
	static int change_focus = FALSE;
	int i, j;


	if (change_focus)
		return;

	if (!gtk_toggle_button_get_active (togglebutton))
	{
		last_togglebutton = NULL;
		last_data = NULL;
	}
	else if (last_togglebutton)
	{
		change_focus = TRUE;
		gtk_toggle_button_set_active (last_togglebutton, FALSE);
		gtk_toggle_button_set_active (togglebutton, FALSE);
		change_focus = FALSE;

		last_togglebutton = NULL;

		i = (int) last_data;
		j = (int) data;

		SWAP (cfg->lpt_leds_order[i], cfg->lpt_leds_order[j], int) show_lpt_leds_order (FALSE);

		lpt_configure (cfg);
	}
	else
	{
		last_togglebutton = togglebutton;
		last_data = data;
	}
}


static void show_lpt_leds_order (int reset)
{
	static GtkWidget *toggle_lpt_led[8];
	char *lpt_led_names[] = {
		"1", "2", "3", "4", "5", "6", "7", "8"
	};
	int i;


	if (reset)
		toggle_lpt_led[0] = NULL;

	if (toggle_lpt_led[0])
	{
		for (i = 0; i < 8; i++)
		{
			gtk_widget_destroy (toggle_lpt_led[i]);
			toggle_lpt_led[i] = NULL;
		}
	}

	for (i = 0; i < 8; i++)
	{
		toggle_lpt_led[i] = gtk_toggle_button_new_with_label (lpt_led_names[cfg->lpt_leds_order[i]]);
		gtk_widget_ref (toggle_lpt_led[i]);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "toggle_lpt_led", toggle_lpt_led[i], (GtkDestroyNotify) gtk_widget_unref);
		gtk_box_pack_start (GTK_BOX (hbox_lpt_order_leds), toggle_lpt_led[i], FALSE, TRUE, 0);
		GTK_WIDGET_UNSET_FLAGS (toggle_lpt_led[i], GTK_CAN_FOCUS);

		gtk_signal_connect (GTK_OBJECT (toggle_lpt_led[i]), "toggled", GTK_SIGNAL_FUNC (on_toggle_lpt_led_toggled), (gpointer) i);
	}

	for (i = 0; i < 8; i++)
		gtk_widget_show (toggle_lpt_led[i]);
}
#endif // NOLPTLEDS


#ifndef NOKEYBOARDLEDS
static void on_check_keyboard_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->keyboard_leds_on = !cfg->keyboard_leds_on;
		keyboard_configure (cfg);
                if (!cfg->keyboard_leds_on)
                    togglebutton->active = FALSE;
	}
}


static void on_check_keyboard_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->keyboard_leds_invert = !cfg->keyboard_leds_invert;
		keyboard_configure (cfg);
	}
}


static void on_check_keyboard_order_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->keyboard_leds_order_invert = !cfg->keyboard_leds_order_invert;
		keyboard_configure (cfg);
	}
}


static void on_combo_keyboard_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data)
{
	cfg->keyboard_leds_scheme = gtk_list_child_position (GTK_LIST (list), widget);
}


static void on_toggle_keyboard_led_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	static GtkToggleButton *last_togglebutton = NULL;
	static gpointer last_data = NULL;
	static int change_focus = FALSE;
	int i, j;


	if (change_focus)
		return;

	if (!gtk_toggle_button_get_active (togglebutton))
	{
		last_togglebutton = NULL;
		last_data = NULL;
	}
	else if (last_togglebutton)
	{
		change_focus = TRUE;
		gtk_toggle_button_set_active (last_togglebutton, FALSE);
		gtk_toggle_button_set_active (togglebutton, FALSE);
		change_focus = FALSE;

		last_togglebutton = NULL;

		i = (int) last_data;
		j = (int) data;

		SWAP (cfg->keyboard_leds_order[i], cfg->keyboard_leds_order[j], int) show_keyboard_leds_order (FALSE);

		keyboard_configure (cfg);
	}
	else
	{
		last_togglebutton = togglebutton;
		last_data = data;
	}
}


static void show_keyboard_leds_order (int reset)
{
	static GtkWidget *toggle_keyboard_led[3];
	char *keyboard_led_names[] = {
		"num", "caps", "scroll"
	};
	int i;


	if (reset)
		toggle_keyboard_led[0] = NULL;

	if (toggle_keyboard_led[0])
	{
		for (i = 0; i < 3; i++)
		{
			gtk_widget_destroy (toggle_keyboard_led[i]);
			toggle_keyboard_led[i] = NULL;
		}
	}

	for (i = 0; i < 3; i++)
	{
		toggle_keyboard_led[i] = gtk_toggle_button_new_with_label (keyboard_led_names[cfg->keyboard_leds_order[i]]);
		gtk_widget_ref (toggle_keyboard_led[i]);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "toggle_keyboard_led", toggle_keyboard_led[i], (GtkDestroyNotify) gtk_widget_unref);
		gtk_box_pack_start (GTK_BOX (hbox_keyboard_order_leds), toggle_keyboard_led[i], FALSE, TRUE, 0);
		GTK_WIDGET_UNSET_FLAGS (toggle_keyboard_led[i], GTK_CAN_FOCUS);

		gtk_signal_connect (GTK_OBJECT (toggle_keyboard_led[i]), "toggled", GTK_SIGNAL_FUNC (on_toggle_keyboard_led_toggled), (gpointer) i);
	}

	for (i = 0; i < 3; i++)
		gtk_widget_show (toggle_keyboard_led[i]);
}
#endif // NOKEYBOARDLEDS


#ifndef NOFLOPPYLEDS
static void on_check_floppy_leds_on_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->floppy_leds_on = !cfg->floppy_leds_on;
		floppy_configure (cfg);
                if (!cfg->floppy_leds_on)
                    togglebutton->active = FALSE;
	}
}


static void on_check_floppy_leds_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->floppy_leds_invert = !cfg->floppy_leds_invert;
		floppy_configure (cfg);
	}
}


static void on_check_floppy_order_inverted_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->floppy_leds_order_invert = !cfg->floppy_leds_order_invert;
		floppy_configure (cfg);
	}
}


static void on_combo_floppy_scheme_list_select_child (GtkList * list, GtkWidget * widget, gpointer data)
{
	cfg->floppy_leds_scheme = gtk_list_child_position (GTK_LIST (list), widget);
}


static void on_toggle_floppy_led_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	static GtkToggleButton *last_togglebutton = NULL;
	static gpointer last_data = NULL;
	static int change_focus = FALSE;
	int i, j;


	if (change_focus)
		return;

	if (!gtk_toggle_button_get_active (togglebutton))
	{
		last_togglebutton = NULL;
		last_data = NULL;
	}
	else if (last_togglebutton)
	{
		change_focus = TRUE;
		gtk_toggle_button_set_active (last_togglebutton, FALSE);
		gtk_toggle_button_set_active (togglebutton, FALSE);
		change_focus = FALSE;

		last_togglebutton = NULL;

		i = (int) last_data;
		j = (int) data;

		SWAP (cfg->floppy_leds_order[i], cfg->floppy_leds_order[j], int) show_floppy_leds_order (FALSE);

		floppy_configure (cfg);
	}
	else
	{
		last_togglebutton = togglebutton;
		last_data = data;
	}
}


static void show_floppy_leds_order (int reset)
{
	static GtkWidget *toggle_floppy_led[FLOPPY_LEDS_COUNT];
	char *floppy_led_names[] = {
		"drive a", "drive b",
	};
	int i;


	if (reset)
		toggle_floppy_led[0] = NULL;

	if (toggle_floppy_led[0])
	{
		for (i = 0; i < FLOPPY_LEDS_COUNT; i++)
		{
			gtk_widget_destroy (toggle_floppy_led[i]);
			toggle_floppy_led[i] = NULL;
		}
	}

	for (i = 0; i < FLOPPY_LEDS_COUNT; i++)
	{
		toggle_floppy_led[i] = gtk_toggle_button_new_with_label (floppy_led_names[cfg->floppy_leds_order[i]]);
		gtk_widget_ref (toggle_floppy_led[i]);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "toggle_floppy_led", toggle_floppy_led[i], (GtkDestroyNotify) gtk_widget_unref);
		gtk_box_pack_start (GTK_BOX (hbox_floppy_order_leds), toggle_floppy_led[i], FALSE, TRUE, 0);
		GTK_WIDGET_UNSET_FLAGS (toggle_floppy_led[i], GTK_CAN_FOCUS);

		gtk_signal_connect (GTK_OBJECT (toggle_floppy_led[i]), "toggled", GTK_SIGNAL_FUNC (on_toggle_floppy_led_toggled), (gpointer) i);
	}

	for (i = 0; i < FLOPPY_LEDS_COUNT; i++)
		gtk_widget_show (toggle_floppy_led[i]);
}
#endif // NOFLOPPYLEDS


static void on_combo_general_filter_list_select_child (GtkList * list, GtkWidget * widget, gpointer data)
{
	cfg->general_filter = gtk_list_child_position (GTK_LIST (list), widget);
}


static void on_check_general_volume_dynamic_toggled (GtkToggleButton * togglebutton, gpointer data)
{
	if (!setting_defaults)
	{
		cfg->general_volume_dynamic = !cfg->general_volume_dynamic;
	}

	gtk_widget_set_sensitive (GTK_WIDGET (hscale_general_volume_max), !cfg->general_volume_dynamic);
}


static void on_adjustment_general_volume_max_value_changed (GtkAdjustment * adjustment, gpointer data)
{
	cfg->general_volume_max = adjustment->value;
}


static void on_adjustment_general_range_low_value_changed (GtkAdjustment * adjustment, gpointer data)
{
	if (adjustment->value == cfg->general_range_high)
		adjustment->value = cfg->general_range_high - 1;

	GTK_ADJUSTMENT (adjustment_general_range_high)->lower = adjustment->value;
	gtk_adjustment_changed (GTK_ADJUSTMENT (adjustment_general_range_high));

	cfg->general_range_low = adjustment->value;
}


static void on_adjustment_general_range_high_value_changed (GtkAdjustment * adjustment, gpointer data)
{
	if (adjustment->value == cfg->general_range_low)
		adjustment->value = cfg->general_range_low + 1;

	GTK_ADJUSTMENT (adjustment_general_range_low)->upper = adjustment->value;
	gtk_adjustment_changed (GTK_ADJUSTMENT (adjustment_general_range_low));

	cfg->general_range_high = adjustment->value;
}



static void create_page_about (void)
{
	GtkWidget *viewport_logo;
	GtkWidget *pixmap_logo;
	GtkWidget *label_about;


	viewport_logo = gtk_viewport_new (NULL, NULL);
	gtk_widget_ref (viewport_logo);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "viewport_logo", viewport_logo, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (viewport_logo);
	gtk_container_add (GTK_CONTAINER (notebook), viewport_logo);
	gtk_container_set_border_width (GTK_CONTAINER (viewport_logo), 13);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (viewport_logo), GTK_SHADOW_ETCHED_OUT);

	if (!pixmap)
		pixmap = gdk_pixmap_create_from_xpm_d (window_config->window, &mask, NULL, led_power_logo_xpm);

	pixmap_logo = gtk_pixmap_new (pixmap, mask);
	gtk_widget_ref (pixmap_logo);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "pixmap_logo", pixmap_logo, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (pixmap_logo);
	gtk_container_add (GTK_CONTAINER (viewport_logo), pixmap_logo);

	label_about = gtk_label_new ("about");
	gtk_widget_ref (label_about);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_about", label_about, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_about);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), notebook_page++), label_about);
}


static void create_page_general (void)
{
	GtkWidget *vbox_general;
	GtkWidget *frame_general_filter;
	GList *combo_general_filter_items = NULL;
	GtkWidget *combo_entry_general_filter;
	GtkWidget *frame_general_volume;
	GtkWidget *vbox_general_volume;
	GtkWidget *hbox_general_volume;
	GtkWidget *label_general_volume_max;
	GtkWidget *frame_general_range;
	GtkWidget *vbox_general_range;
	GtkWidget *hbox_general_range_low;
	GtkWidget *label_general_range_low;
	GtkWidget *hscale_general_range_low;
	GtkWidget *hbox_general_range_high;
	GtkWidget *label_general_range_high;
	GtkWidget *hscale_general_range_high;
	GtkWidget *label_general;


	vbox_general = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_general);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_general", vbox_general, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_general);
	gtk_container_add (GTK_CONTAINER (notebook), vbox_general);

	frame_general_filter = gtk_frame_new ("filter");
	gtk_widget_ref (frame_general_filter);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_general_filter", frame_general_filter, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_general_filter);
	gtk_box_pack_start (GTK_BOX (vbox_general), frame_general_filter, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_general_filter), 8);

	combo_general_filter = gtk_combo_new ();
	gtk_widget_ref (combo_general_filter);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_general_filter", combo_general_filter, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_general_filter);
	gtk_container_add (GTK_CONTAINER (frame_general_filter), combo_general_filter);
	gtk_container_set_border_width (GTK_CONTAINER (combo_general_filter), 4);

	{
		FreqFilter *filters;
		int filters_count, i;

		filters_count = sp_get_filters (&filters);
		for (i = 0; i < filters_count; i++)
			combo_general_filter_items = g_list_append (combo_general_filter_items, filters[i].name);
	}

	gtk_combo_set_popdown_strings (GTK_COMBO (combo_general_filter), combo_general_filter_items);
	g_list_free (combo_general_filter_items);

	combo_entry_general_filter = GTK_COMBO (combo_general_filter)->entry;
	gtk_widget_ref (combo_entry_general_filter);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "combo_entry_general_filter", combo_entry_general_filter, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_entry_general_filter);
	GTK_WIDGET_UNSET_FLAGS (combo_entry_general_filter, GTK_CAN_FOCUS);
	gtk_entry_set_editable (GTK_ENTRY (combo_entry_general_filter), FALSE);

	frame_general_volume = gtk_frame_new ("volume");
	gtk_widget_ref (frame_general_volume);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_general_volume", frame_general_volume, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_general_volume);
	gtk_box_pack_start (GTK_BOX (vbox_general), frame_general_volume, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_general_volume), 4);

	vbox_general_volume = gtk_vbox_new (FALSE, 4);
	gtk_widget_ref (vbox_general_volume);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_general_volume", vbox_general_volume, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_general_volume);
	gtk_container_add (GTK_CONTAINER (frame_general_volume), vbox_general_volume);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_general_volume), 4);

	check_general_volume_dynamic = gtk_check_button_new_with_label ("dynamic");
	gtk_widget_ref (check_general_volume_dynamic);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "check_general_volume_dynamic", check_general_volume_dynamic, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_general_volume_dynamic);
	gtk_box_pack_start (GTK_BOX (vbox_general_volume), check_general_volume_dynamic, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_general_volume_dynamic, GTK_CAN_FOCUS);

	hbox_general_volume = gtk_hbox_new (FALSE, 8);
	gtk_widget_ref (hbox_general_volume);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_general_volume", hbox_general_volume, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox_general_volume);
	gtk_box_pack_start (GTK_BOX (vbox_general_volume), hbox_general_volume, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox_general_volume), 8);

	label_general_volume_max = gtk_label_new ("max");
	gtk_widget_ref (label_general_volume_max);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_general_volume_max", label_general_volume_max, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_general_volume_max);
	gtk_box_pack_start (GTK_BOX (hbox_general_volume), label_general_volume_max, FALSE, FALSE, 0);

	adjustment_general_volume_max = gtk_adjustment_new (255, 0, 255, 1, 0, 0);
	hscale_general_volume_max = gtk_hscale_new (GTK_ADJUSTMENT (adjustment_general_volume_max));
	gtk_widget_ref (hscale_general_volume_max);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "hscale_general_volume_max", hscale_general_volume_max, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hscale_general_volume_max);
	gtk_box_pack_start (GTK_BOX (hbox_general_volume), hscale_general_volume_max, TRUE, TRUE, 0);
	GTK_WIDGET_UNSET_FLAGS (hscale_general_volume_max, GTK_CAN_FOCUS);
	gtk_scale_set_digits (GTK_SCALE (hscale_general_volume_max), 0);
	gtk_scale_set_value_pos (GTK_SCALE (hscale_general_volume_max), GTK_POS_RIGHT);

	frame_general_range = gtk_frame_new ("range");
	gtk_widget_ref (frame_general_range);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_general_range", frame_general_range, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_general_range);
	gtk_box_pack_start (GTK_BOX (vbox_general), frame_general_range, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_general_range), 4);

	vbox_general_range = gtk_vbox_new (FALSE, 4);
	gtk_widget_ref (vbox_general_range);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_general_range", vbox_general_range, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_general_range);
	gtk_container_add (GTK_CONTAINER (frame_general_range), vbox_general_range);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_general_range), 4);

	hbox_general_range_low = gtk_hbox_new (FALSE, 8);
	gtk_widget_ref (hbox_general_range_low);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_general_range_low", hbox_general_range_low, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox_general_range_low);
	gtk_box_pack_start (GTK_BOX (vbox_general_range), hbox_general_range_low, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox_general_range_low), 8);

	label_general_range_low = gtk_label_new ("lo");
	gtk_widget_ref (label_general_range_low);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_general_range_low", label_general_range_low, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_general_range_low);
	gtk_box_pack_start (GTK_BOX (hbox_general_range_low), label_general_range_low, FALSE, FALSE, 0);

	adjustment_general_range_low = gtk_adjustment_new (0, 0, 255, 1, 0, 0);
	hscale_general_range_low = gtk_hscale_new (GTK_ADJUSTMENT (adjustment_general_range_low));
	gtk_widget_ref (hscale_general_range_low);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "hscale_general_range_low", hscale_general_range_low, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hscale_general_range_low);
	gtk_box_pack_start (GTK_BOX (hbox_general_range_low), hscale_general_range_low, TRUE, TRUE, 0);
	GTK_WIDGET_UNSET_FLAGS (hscale_general_range_low, GTK_CAN_FOCUS);
	gtk_scale_set_digits (GTK_SCALE (hscale_general_range_low), 0);
	gtk_scale_set_value_pos (GTK_SCALE (hscale_general_range_low), GTK_POS_RIGHT);

	hbox_general_range_high = gtk_hbox_new (FALSE, 8);
	gtk_widget_ref (hbox_general_range_high);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_general_range_high", hbox_general_range_high, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox_general_range_high);
	gtk_box_pack_start (GTK_BOX (vbox_general_range), hbox_general_range_high, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox_general_range_high), 8);

	label_general_range_high = gtk_label_new ("hi");
	gtk_widget_ref (label_general_range_high);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_general_range_high", label_general_range_high, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_general_range_high);
	gtk_box_pack_start (GTK_BOX (hbox_general_range_high), label_general_range_high, FALSE, FALSE, 0);

	adjustment_general_range_high = gtk_adjustment_new (255, 0, 255, 1, 0, 0);
	hscale_general_range_high = gtk_hscale_new (GTK_ADJUSTMENT (adjustment_general_range_high));
	gtk_widget_ref (hscale_general_range_high);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "hscale_general_range_high", hscale_general_range_high, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hscale_general_range_high);
	gtk_box_pack_start (GTK_BOX (hbox_general_range_high), hscale_general_range_high, TRUE, TRUE, 0);
	GTK_WIDGET_UNSET_FLAGS (hscale_general_range_high, GTK_CAN_FOCUS);
	gtk_scale_set_digits (GTK_SCALE (hscale_general_range_high), 0);
	gtk_scale_set_value_pos (GTK_SCALE (hscale_general_range_high), GTK_POS_RIGHT);

//	label_general = gtk_label_new ("general");
	label_general = gtk_label_new ("adv");
	gtk_widget_ref (label_general);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_general", label_general, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_general);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), notebook_page++), label_general);


	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo_general_filter)->list),
						"select_child", GTK_SIGNAL_FUNC (on_combo_general_filter_list_select_child), NULL);
	gtk_signal_connect (GTK_OBJECT (check_general_volume_dynamic), "toggled", GTK_SIGNAL_FUNC (on_check_general_volume_dynamic_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (adjustment_general_volume_max),
						"value_changed", GTK_SIGNAL_FUNC (on_adjustment_general_volume_max_value_changed), NULL);
	gtk_signal_connect (GTK_OBJECT (adjustment_general_range_low),
						"value_changed", GTK_SIGNAL_FUNC (on_adjustment_general_range_low_value_changed), NULL);
	gtk_signal_connect (GTK_OBJECT (adjustment_general_range_high),
						"value_changed", GTK_SIGNAL_FUNC (on_adjustment_general_range_high_value_changed), NULL);
}


#ifndef NOSCREENLEDS
static void create_page_screen (void)
{
	GtkWidget *vbox_screen;
	GtkWidget *frame_screen_leds;
	GtkWidget *vbox_screen_leds;
	GtkWidget *frame_screen_order;
	GtkWidget *vbox_screen_order;
	GtkWidget *frame_screen_scheme;
	GList *combo_screen_scheme_items = NULL;
	GtkWidget *combo_entry_screen_scheme;
	GtkWidget *label_screen;
	int leds_count;
	LedDevice *devices;


	vbox_screen = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_screen);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_screen", vbox_screen, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_screen);
	gtk_container_add (GTK_CONTAINER (notebook), vbox_screen);

	frame_screen_leds = gtk_frame_new ("leds");
	gtk_widget_ref (frame_screen_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_screen_leds", frame_screen_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_screen_leds);
	gtk_box_pack_start (GTK_BOX (vbox_screen), frame_screen_leds, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_screen_leds), 8);

	vbox_screen_leds = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_screen_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_screen_leds", vbox_screen_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_screen_leds);
	gtk_container_add (GTK_CONTAINER (frame_screen_leds), vbox_screen_leds);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_screen_leds), 4);

	check_screen_leds_on = gtk_check_button_new_with_label ("on");
	gtk_widget_ref (check_screen_leds_on);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "check_screen_leds_on", check_screen_leds_on, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_screen_leds_on);
	gtk_box_pack_start (GTK_BOX (vbox_screen_leds), check_screen_leds_on, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_screen_leds_on, GTK_CAN_FOCUS);

	check_screen_leds_inverted = gtk_check_button_new_with_label ("inverted");
	gtk_widget_ref (check_screen_leds_inverted);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "check_screen_leds_inverted", check_screen_leds_inverted, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_screen_leds_inverted);
	gtk_box_pack_start (GTK_BOX (vbox_screen_leds), check_screen_leds_inverted, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_screen_leds_inverted, GTK_CAN_FOCUS);

	vis_get_devices (&devices);
	leds_count = devices[SCREENLEDS_N].leds_count;

	if (leds_count > 1)
	{
		frame_screen_order = gtk_frame_new ("order");
		gtk_widget_ref (frame_screen_order);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_screen_order", frame_screen_order, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (frame_screen_order);
		gtk_box_pack_start (GTK_BOX (vbox_screen), frame_screen_order, FALSE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame_screen_order), 8);

		vbox_screen_order = gtk_vbox_new (FALSE, 4);
		gtk_widget_ref (vbox_screen_order);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_screen_order", vbox_screen_order, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (vbox_screen_order);
		gtk_container_add (GTK_CONTAINER (frame_screen_order), vbox_screen_order);
		gtk_container_set_border_width (GTK_CONTAINER (vbox_screen_order), 4);

		check_screen_order_inverted = gtk_check_button_new_with_label ("inverted");
		gtk_widget_ref (check_screen_order_inverted);
		gtk_object_set_data_full (GTK_OBJECT (window_config),
								  "check_screen_order_inverted", check_screen_order_inverted, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (check_screen_order_inverted);
		gtk_box_pack_start (GTK_BOX (vbox_screen_order), check_screen_order_inverted, FALSE, FALSE, 0);
		GTK_WIDGET_UNSET_FLAGS (check_screen_order_inverted, GTK_CAN_FOCUS);

		hbox_screen_order_leds = gtk_hbox_new (TRUE, 0);
		gtk_widget_ref (hbox_screen_order_leds);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_screen_order_leds", hbox_screen_order_leds, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (hbox_screen_order_leds);
		gtk_box_pack_start (GTK_BOX (vbox_screen_order), hbox_screen_order_leds, FALSE, TRUE, 0);

		show_screen_leds_order (TRUE);
	}

	frame_screen_scheme = gtk_frame_new ("scheme");
	gtk_widget_ref (frame_screen_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_screen_scheme", frame_screen_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_screen_scheme);
	gtk_box_pack_start (GTK_BOX (vbox_screen), frame_screen_scheme, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_screen_scheme), 8);

	combo_screen_scheme = gtk_combo_new ();
	gtk_widget_ref (combo_screen_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_screen_scheme", combo_screen_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_screen_scheme);
	gtk_container_add (GTK_CONTAINER (frame_screen_scheme), combo_screen_scheme);
	gtk_container_set_border_width (GTK_CONTAINER (combo_screen_scheme), 4);

	{
		Scheme *schemes;
		int schemes_count, i;

		schemes_count = sp_get_schemes (&schemes);
		for (i = 0; i < schemes_count; i++)
			combo_screen_scheme_items = g_list_append (combo_screen_scheme_items, schemes[i].name);
	}

	gtk_combo_set_popdown_strings (GTK_COMBO (combo_screen_scheme), combo_screen_scheme_items);
	g_list_free (combo_screen_scheme_items);

	combo_entry_screen_scheme = GTK_COMBO (combo_screen_scheme)->entry;
	gtk_widget_ref (combo_entry_screen_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "combo_entry_screen_scheme", combo_entry_screen_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_entry_screen_scheme);
	GTK_WIDGET_UNSET_FLAGS (combo_entry_screen_scheme, GTK_CAN_FOCUS);
	gtk_entry_set_editable (GTK_ENTRY (combo_entry_screen_scheme), FALSE);

	label_screen = gtk_label_new ("screen");
	gtk_widget_ref (label_screen);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_screen", label_screen, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_screen);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), notebook_page++), label_screen);


	gtk_signal_connect (GTK_OBJECT (check_screen_leds_on), "toggled", GTK_SIGNAL_FUNC (on_check_screen_leds_on_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (check_screen_leds_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_screen_leds_inverted_toggled), NULL);

	if (check_screen_order_inverted)
		gtk_signal_connect (GTK_OBJECT (check_screen_order_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_screen_order_inverted_toggled), NULL);

	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo_screen_scheme)->list),
						"select_child", GTK_SIGNAL_FUNC (on_combo_screen_scheme_list_select_child), NULL);
}
#endif


#ifndef NOLPTLEDS
static void create_page_lpt (void)
{
	GtkWidget *vbox_lpt;
	GtkWidget *frame_lpt_leds;
	GtkWidget *vbox_lpt_leds;
	GtkWidget *frame_lpt_order;
	GtkWidget *vbox_lpt_order;
	GtkWidget *frame_lpt_scheme;
	GList *combo_lpt_scheme_items = NULL;
	GtkWidget *combo_entry_lpt_scheme;
	GtkWidget *hbox_lpt_port;
	GtkWidget *label_lpt_port;
	GList *combo_lpt_port_items = NULL;
	GtkWidget *combo_entry_lpt_port;
	GtkWidget *label_lpt;


	vbox_lpt = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_lpt);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_lpt", vbox_lpt, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_lpt);
	gtk_container_add (GTK_CONTAINER (notebook), vbox_lpt);

	frame_lpt_leds = gtk_frame_new ("leds");
	gtk_widget_ref (frame_lpt_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_lpt_leds", frame_lpt_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_lpt_leds);
	gtk_box_pack_start (GTK_BOX (vbox_lpt), frame_lpt_leds, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_lpt_leds), 8);

	vbox_lpt_leds = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_lpt_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_lpt_leds", vbox_lpt_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_lpt_leds);
	gtk_container_add (GTK_CONTAINER (frame_lpt_leds), vbox_lpt_leds);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_lpt_leds), 4);

	check_lpt_leds_on = gtk_check_button_new_with_label ("on");
	gtk_widget_ref (check_lpt_leds_on);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "check_lpt_leds_on", check_lpt_leds_on, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_lpt_leds_on);
	gtk_box_pack_start (GTK_BOX (vbox_lpt_leds), check_lpt_leds_on, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_lpt_leds_on, GTK_CAN_FOCUS);

	check_lpt_leds_inverted = gtk_check_button_new_with_label ("inverted");
	gtk_widget_ref (check_lpt_leds_inverted);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "check_lpt_leds_inverted", check_lpt_leds_inverted, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_lpt_leds_inverted);
	gtk_box_pack_start (GTK_BOX (vbox_lpt_leds), check_lpt_leds_inverted, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_lpt_leds_inverted, GTK_CAN_FOCUS);

	frame_lpt_order = gtk_frame_new ("order");
	gtk_widget_ref (frame_lpt_order);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_lpt_order", frame_lpt_order, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_lpt_order);
	gtk_box_pack_start (GTK_BOX (vbox_lpt), frame_lpt_order, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_lpt_order), 8);

	vbox_lpt_order = gtk_vbox_new (FALSE, 4);
	gtk_widget_ref (vbox_lpt_order);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_lpt_order", vbox_lpt_order, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_lpt_order);
	gtk_container_add (GTK_CONTAINER (frame_lpt_order), vbox_lpt_order);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_lpt_order), 4);

	check_lpt_order_inverted = gtk_check_button_new_with_label ("inverted");
	gtk_widget_ref (check_lpt_order_inverted);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "check_lpt_order_inverted", check_lpt_order_inverted, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_lpt_order_inverted);
	gtk_box_pack_start (GTK_BOX (vbox_lpt_order), check_lpt_order_inverted, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_lpt_order_inverted, GTK_CAN_FOCUS);

	hbox_lpt_order_leds = gtk_hbox_new (TRUE, 0);
	gtk_widget_ref (hbox_lpt_order_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_lpt_order_leds", hbox_lpt_order_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox_lpt_order_leds);
	gtk_box_pack_start (GTK_BOX (vbox_lpt_order), hbox_lpt_order_leds, FALSE, TRUE, 0);

	show_lpt_leds_order (TRUE);

	frame_lpt_scheme = gtk_frame_new ("scheme");
	gtk_widget_ref (frame_lpt_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_lpt_scheme", frame_lpt_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_lpt_scheme);
	gtk_box_pack_start (GTK_BOX (vbox_lpt), frame_lpt_scheme, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_lpt_scheme), 8);

	combo_lpt_scheme = gtk_combo_new ();
	gtk_widget_ref (combo_lpt_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_lpt_scheme", combo_lpt_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_lpt_scheme);
	gtk_container_add (GTK_CONTAINER (frame_lpt_scheme), combo_lpt_scheme);
	gtk_container_set_border_width (GTK_CONTAINER (combo_lpt_scheme), 4);

	{
		Scheme *schemes;
		int schemes_count, i;

		schemes_count = sp_get_schemes (&schemes);
		for (i = 0; i < schemes_count; i++)
			combo_lpt_scheme_items = g_list_append (combo_lpt_scheme_items, schemes[i].name);
	}

	gtk_combo_set_popdown_strings (GTK_COMBO (combo_lpt_scheme), combo_lpt_scheme_items);
	g_list_free (combo_lpt_scheme_items);

	combo_entry_lpt_scheme = GTK_COMBO (combo_lpt_scheme)->entry;
	gtk_widget_ref (combo_entry_lpt_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_entry_lpt_scheme", combo_entry_lpt_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_entry_lpt_scheme);
	GTK_WIDGET_UNSET_FLAGS (combo_entry_lpt_scheme, GTK_CAN_FOCUS);
	gtk_entry_set_editable (GTK_ENTRY (combo_entry_lpt_scheme), FALSE);

	hbox_lpt_port = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox_lpt_port);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_lpt_port", hbox_lpt_port, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox_lpt_port);
	gtk_box_pack_start (GTK_BOX (vbox_lpt), hbox_lpt_port, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox_lpt_port), 8);

	label_lpt_port = gtk_label_new ("port");
	gtk_widget_ref (label_lpt_port);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_lpt_port", label_lpt_port, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_lpt_port);
	gtk_box_pack_start (GTK_BOX (hbox_lpt_port), label_lpt_port, FALSE, FALSE, 0);

	combo_lpt_port = gtk_combo_new ();
	gtk_widget_ref (combo_lpt_port);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_lpt_port", combo_lpt_port, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_lpt_port);
	gtk_box_pack_start (GTK_BOX (hbox_lpt_port), combo_lpt_port, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (combo_lpt_port), 2);

	{
		int i;
		char *lpt_port_addresses[] = {
			"0x0278", "0x0378", "0x02BC", "0x03BC"
		};

		for (i = 0; i < ARRAY_SIZE (lpt_port_addresses); i++)
			combo_lpt_port_items = g_list_append (combo_lpt_port_items, lpt_port_addresses[i]);
	}

	gtk_combo_set_popdown_strings (GTK_COMBO (combo_lpt_port), combo_lpt_port_items);
	g_list_free (combo_lpt_port_items);

	combo_entry_lpt_port = GTK_COMBO (combo_lpt_port)->entry;
	gtk_widget_ref (combo_entry_lpt_port);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_entry_lpt_port", combo_entry_lpt_port, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_entry_lpt_port);
	GTK_WIDGET_UNSET_FLAGS (combo_entry_lpt_port, GTK_CAN_FOCUS);
	gtk_entry_set_editable (GTK_ENTRY (combo_entry_lpt_port), FALSE);

	label_lpt = gtk_label_new ("lpt");
	gtk_widget_ref (label_lpt);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_lpt", label_lpt, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_lpt);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), notebook_page++), label_lpt);


	gtk_signal_connect (GTK_OBJECT (check_lpt_leds_on), "toggled", GTK_SIGNAL_FUNC (on_check_lpt_leds_on_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (check_lpt_leds_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_lpt_leds_inverted_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (check_lpt_order_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_lpt_order_inverted_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo_lpt_scheme)->list),
						"select_child", GTK_SIGNAL_FUNC (on_combo_lpt_scheme_list_select_child), NULL);
	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo_lpt_port)->list), "select_child", GTK_SIGNAL_FUNC (on_combo_lpt_port_list_select_child), NULL);
}
#endif // NOLPTLEDS


#ifndef NOKEYBOARDLEDS
static void create_page_keyboard (void)
{
	GtkWidget *vbox_keyboard;
	GtkWidget *frame_keyboard_leds;
	GtkWidget *vbox_keyboard_leds;
	GtkWidget *frame_keyboard_order;
	GtkWidget *vbox_keyboard_order;
	GtkWidget *frame_keyboard_scheme;
	GList *combo_keyboard_scheme_items = NULL;
	GtkWidget *combo_entry_keyboard_scheme;
	GtkWidget *label_keyboard;


	vbox_keyboard = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_keyboard);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_keyboard", vbox_keyboard, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_keyboard);
	gtk_container_add (GTK_CONTAINER (notebook), vbox_keyboard);

	frame_keyboard_leds = gtk_frame_new ("leds");
	gtk_widget_ref (frame_keyboard_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_keyboard_leds", frame_keyboard_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_keyboard_leds);
	gtk_box_pack_start (GTK_BOX (vbox_keyboard), frame_keyboard_leds, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_keyboard_leds), 8);

	vbox_keyboard_leds = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_keyboard_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_keyboard_leds", vbox_keyboard_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_keyboard_leds);
	gtk_container_add (GTK_CONTAINER (frame_keyboard_leds), vbox_keyboard_leds);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_keyboard_leds), 4);

	check_keyboard_leds_on = gtk_check_button_new_with_label ("on");
	gtk_widget_ref (check_keyboard_leds_on);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "check_keyboard_leds_on", check_keyboard_leds_on, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_keyboard_leds_on);
	gtk_box_pack_start (GTK_BOX (vbox_keyboard_leds), check_keyboard_leds_on, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_keyboard_leds_on, GTK_CAN_FOCUS);

	check_keyboard_leds_inverted = gtk_check_button_new_with_label ("inverted");
	gtk_widget_ref (check_keyboard_leds_inverted);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "check_keyboard_leds_inverted", check_keyboard_leds_inverted, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_keyboard_leds_inverted);
	gtk_box_pack_start (GTK_BOX (vbox_keyboard_leds), check_keyboard_leds_inverted, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_keyboard_leds_inverted, GTK_CAN_FOCUS);

	frame_keyboard_order = gtk_frame_new ("order");
	gtk_widget_ref (frame_keyboard_order);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_keyboard_order", frame_keyboard_order, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_keyboard_order);
	gtk_box_pack_start (GTK_BOX (vbox_keyboard), frame_keyboard_order, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_keyboard_order), 8);

	vbox_keyboard_order = gtk_vbox_new (FALSE, 4);
	gtk_widget_ref (vbox_keyboard_order);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_keyboard_order", vbox_keyboard_order, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_keyboard_order);
	gtk_container_add (GTK_CONTAINER (frame_keyboard_order), vbox_keyboard_order);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_keyboard_order), 4);

	check_keyboard_order_inverted = gtk_check_button_new_with_label ("inverted");
	gtk_widget_ref (check_keyboard_order_inverted);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "check_keyboard_order_inverted", check_keyboard_order_inverted, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_keyboard_order_inverted);

	gtk_box_pack_start (GTK_BOX (vbox_keyboard_order), check_keyboard_order_inverted, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_keyboard_order_inverted, GTK_CAN_FOCUS);

	hbox_keyboard_order_leds = gtk_hbox_new (TRUE, 0);
	gtk_widget_ref (hbox_keyboard_order_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_keyboard_order_leds", hbox_keyboard_order_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox_keyboard_order_leds);
	gtk_box_pack_start (GTK_BOX (vbox_keyboard_order), hbox_keyboard_order_leds, FALSE, TRUE, 0);

	show_keyboard_leds_order (TRUE);

	frame_keyboard_scheme = gtk_frame_new ("scheme");
	gtk_widget_ref (frame_keyboard_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_keyboard_scheme", frame_keyboard_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_keyboard_scheme);
	gtk_box_pack_start (GTK_BOX (vbox_keyboard), frame_keyboard_scheme, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_keyboard_scheme), 8);

	combo_keyboard_scheme = gtk_combo_new ();
	gtk_widget_ref (combo_keyboard_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_keyboard_scheme", combo_keyboard_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_keyboard_scheme);
	gtk_container_add (GTK_CONTAINER (frame_keyboard_scheme), combo_keyboard_scheme);
	gtk_container_set_border_width (GTK_CONTAINER (combo_keyboard_scheme), 4);

	{
		Scheme *schemes;
		int schemes_count, i;

		schemes_count = sp_get_schemes (&schemes);
		for (i = 0; i < schemes_count; i++)
			combo_keyboard_scheme_items = g_list_append (combo_keyboard_scheme_items, schemes[i].name);
	}

	gtk_combo_set_popdown_strings (GTK_COMBO (combo_keyboard_scheme), combo_keyboard_scheme_items);
	g_list_free (combo_keyboard_scheme_items);

	combo_entry_keyboard_scheme = GTK_COMBO (combo_keyboard_scheme)->entry;
	gtk_widget_ref (combo_entry_keyboard_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "combo_entry_keyboard_scheme", combo_entry_keyboard_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_entry_keyboard_scheme);
	GTK_WIDGET_UNSET_FLAGS (combo_entry_keyboard_scheme, GTK_CAN_FOCUS);
	gtk_entry_set_editable (GTK_ENTRY (combo_entry_keyboard_scheme), FALSE);

	label_keyboard = gtk_label_new ("keyboard");
	gtk_widget_ref (label_keyboard);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_keyboard", label_keyboard, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_keyboard);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), notebook_page++), label_keyboard);


	gtk_signal_connect (GTK_OBJECT (check_keyboard_leds_on), "toggled", GTK_SIGNAL_FUNC (on_check_keyboard_leds_on_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (check_keyboard_leds_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_keyboard_leds_inverted_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (check_keyboard_order_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_keyboard_order_inverted_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo_keyboard_scheme)->list),
						"select_child", GTK_SIGNAL_FUNC (on_combo_keyboard_scheme_list_select_child), NULL);
}
#endif // NOKEYBOARDLEDS


#ifndef NOFLOPPYLEDS
static void create_page_floppy (void)
{
	GtkWidget *vbox_floppy;
	GtkWidget *frame_floppy_leds;
	GtkWidget *vbox_floppy_leds;
	GtkWidget *frame_floppy_order;
	GtkWidget *vbox_floppy_order;
	GtkWidget *frame_floppy_scheme;
	GList *combo_floppy_scheme_items = NULL;
	GtkWidget *combo_entry_floppy_scheme;
	GtkWidget *label_floppy;
	int leds_count;
	LedDevice *devices;


	vbox_floppy = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_floppy);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_floppy", vbox_floppy, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_floppy);
	gtk_container_add (GTK_CONTAINER (notebook), vbox_floppy);

	frame_floppy_leds = gtk_frame_new ("leds");
	gtk_widget_ref (frame_floppy_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_floppy_leds", frame_floppy_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_floppy_leds);
	gtk_box_pack_start (GTK_BOX (vbox_floppy), frame_floppy_leds, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_floppy_leds), 8);

	vbox_floppy_leds = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_floppy_leds);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_floppy_leds", vbox_floppy_leds, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_floppy_leds);
	gtk_container_add (GTK_CONTAINER (frame_floppy_leds), vbox_floppy_leds);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_floppy_leds), 4);

	check_floppy_leds_on = gtk_check_button_new_with_label ("on");
	gtk_widget_ref (check_floppy_leds_on);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "check_floppy_leds_on", check_floppy_leds_on, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_floppy_leds_on);
	gtk_box_pack_start (GTK_BOX (vbox_floppy_leds), check_floppy_leds_on, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_floppy_leds_on, GTK_CAN_FOCUS);

	check_floppy_leds_inverted = gtk_check_button_new_with_label ("inverted");
	gtk_widget_ref (check_floppy_leds_inverted);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "check_floppy_leds_inverted", check_floppy_leds_inverted, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (check_floppy_leds_inverted);
	gtk_box_pack_start (GTK_BOX (vbox_floppy_leds), check_floppy_leds_inverted, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (check_floppy_leds_inverted, GTK_CAN_FOCUS);

	vis_get_devices (&devices);
	leds_count = devices[FLOPPYLEDS_N].leds_count;

	if (leds_count > 1)
	{
		frame_floppy_order = gtk_frame_new ("order");
		gtk_widget_ref (frame_floppy_order);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_floppy_order", frame_floppy_order, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (frame_floppy_order);
		gtk_box_pack_start (GTK_BOX (vbox_floppy), frame_floppy_order, FALSE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame_floppy_order), 8);

		vbox_floppy_order = gtk_vbox_new (FALSE, 4);
		gtk_widget_ref (vbox_floppy_order);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_floppy_order", vbox_floppy_order, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (vbox_floppy_order);
		gtk_container_add (GTK_CONTAINER (frame_floppy_order), vbox_floppy_order);
		gtk_container_set_border_width (GTK_CONTAINER (vbox_floppy_order), 4);

		check_floppy_order_inverted = gtk_check_button_new_with_label ("inverted");
		gtk_widget_ref (check_floppy_order_inverted);
		gtk_object_set_data_full (GTK_OBJECT (window_config),
								  "check_floppy_order_inverted", check_floppy_order_inverted, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (check_floppy_order_inverted);

		gtk_box_pack_start (GTK_BOX (vbox_floppy_order), check_floppy_order_inverted, FALSE, FALSE, 0);
		GTK_WIDGET_UNSET_FLAGS (check_floppy_order_inverted, GTK_CAN_FOCUS);

		hbox_floppy_order_leds = gtk_hbox_new (TRUE, 0);
		gtk_widget_ref (hbox_floppy_order_leds);
		gtk_object_set_data_full (GTK_OBJECT (window_config), "hbox_floppy_order_leds", hbox_floppy_order_leds, (GtkDestroyNotify) gtk_widget_unref);
		gtk_widget_show (hbox_floppy_order_leds);
		gtk_box_pack_start (GTK_BOX (vbox_floppy_order), hbox_floppy_order_leds, FALSE, TRUE, 0);

		show_floppy_leds_order (TRUE);
	}

	frame_floppy_scheme = gtk_frame_new ("scheme");
	gtk_widget_ref (frame_floppy_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "frame_floppy_scheme", frame_floppy_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame_floppy_scheme);
	gtk_box_pack_start (GTK_BOX (vbox_floppy), frame_floppy_scheme, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_floppy_scheme), 8);

	combo_floppy_scheme = gtk_combo_new ();
	gtk_widget_ref (combo_floppy_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "combo_floppy_scheme", combo_floppy_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_floppy_scheme);
	gtk_container_add (GTK_CONTAINER (frame_floppy_scheme), combo_floppy_scheme);
	gtk_container_set_border_width (GTK_CONTAINER (combo_floppy_scheme), 4);

	{
		Scheme *schemes;
		int schemes_count, i;

		schemes_count = sp_get_schemes (&schemes);
		for (i = 0; i < schemes_count; i++)
			combo_floppy_scheme_items = g_list_append (combo_floppy_scheme_items, schemes[i].name);
	}

	gtk_combo_set_popdown_strings (GTK_COMBO (combo_floppy_scheme), combo_floppy_scheme_items);
	g_list_free (combo_floppy_scheme_items);

	combo_entry_floppy_scheme = GTK_COMBO (combo_floppy_scheme)->entry;
	gtk_widget_ref (combo_entry_floppy_scheme);
	gtk_object_set_data_full (GTK_OBJECT (window_config),
							  "combo_entry_floppy_scheme", combo_entry_floppy_scheme, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (combo_entry_floppy_scheme);
	GTK_WIDGET_UNSET_FLAGS (combo_entry_floppy_scheme, GTK_CAN_FOCUS);
	gtk_entry_set_editable (GTK_ENTRY (combo_entry_floppy_scheme), FALSE);

	label_floppy = gtk_label_new ("floppy");
	gtk_widget_ref (label_floppy);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "label_floppy", label_floppy, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label_floppy);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), notebook_page++), label_floppy);


	gtk_signal_connect (GTK_OBJECT (check_floppy_leds_on), "toggled", GTK_SIGNAL_FUNC (on_check_floppy_leds_on_toggled), NULL);
	gtk_signal_connect (GTK_OBJECT (check_floppy_leds_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_floppy_leds_inverted_toggled), NULL);

	if (check_floppy_order_inverted)
		gtk_signal_connect (GTK_OBJECT (check_floppy_order_inverted), "toggled", GTK_SIGNAL_FUNC (on_check_floppy_order_inverted_toggled), NULL);

	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo_floppy_scheme)->list),
						"select_child", GTK_SIGNAL_FUNC (on_combo_floppy_scheme_list_select_child), NULL);
}
#endif // NOFLOPPYLEDS



static void create_window_config (void)
{
	GtkWidget *button_close;
	GtkWidget *vbox_main;


	window_config = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (window_config), "window_config", window_config);
	gtk_widget_set_usize (window_config, 308, 364);
	gtk_window_set_title (GTK_WINDOW (window_config), "Led Power");
	gtk_window_set_policy (GTK_WINDOW (window_config), FALSE, FALSE, FALSE);
	gtk_widget_realize (window_config);

	vbox_main = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox_main);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "vbox_main", vbox_main, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox_main);
	gtk_container_add (GTK_CONTAINER (window_config), vbox_main);
	gtk_container_set_border_width (GTK_CONTAINER (vbox_main), 6);

	notebook = gtk_notebook_new ();
	gtk_widget_ref (notebook);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "notebook", notebook, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (notebook);
	gtk_box_pack_start (GTK_BOX (vbox_main), notebook, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (notebook), 4);

	button_close = gtk_button_new_with_label ("close");
	gtk_widget_ref (button_close);
	gtk_object_set_data_full (GTK_OBJECT (window_config), "button_close", button_close, (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (button_close);
	gtk_box_pack_start (GTK_BOX (vbox_main), button_close, FALSE, FALSE, 0);
	GTK_WIDGET_UNSET_FLAGS (button_close, GTK_CAN_FOCUS);


	gtk_signal_connect (GTK_OBJECT (button_close), "clicked", GTK_SIGNAL_FUNC (on_button_close_clicked), NULL);
	gtk_signal_connect (GTK_OBJECT (window_config), "destroy", GTK_SIGNAL_FUNC (on_window_config_destroy), NULL);

	notebook_page = 0;

	create_page_about ();
#ifndef NOSCREENLEDS
	create_page_screen ();
#endif
#ifndef NOLPTLEDS
	create_page_lpt ();
#endif
#ifndef NOKEYBOARDLEDS
	create_page_keyboard ();
#endif
#ifndef NOFLOPPYLEDS
	create_page_floppy ();
#endif
	create_page_general ();
}



static void config_dialog_set_defaults (Configuration * cfg)
{
	setting_defaults = TRUE;

#ifndef NOSCREENLEDS
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_screen_leds_on), cfg->screen_leds_on);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_screen_leds_inverted), cfg->screen_leds_invert);

	if (check_screen_order_inverted)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_screen_order_inverted), cfg->screen_leds_order_invert);

	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_screen_scheme)->list), cfg->screen_leds_scheme);
#endif

#ifndef NOLPTLEDS
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_lpt_leds_on), cfg->lpt_leds_on);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_lpt_leds_inverted), cfg->lpt_leds_invert);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_lpt_order_inverted), cfg->lpt_leds_order_invert);
	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_lpt_scheme)->list), cfg->lpt_leds_scheme);
	switch (cfg->lpt_port_address)
	{
		case 0x0278:
			gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_lpt_port)->list), 0);
			break;

		case 0x0378:
			gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_lpt_port)->list), 1);
			break;

		case 0x02BC:
			gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_lpt_port)->list), 2);
			break;

		case 0x03BC:
			gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_lpt_port)->list), 3);
			break;

		default:
			gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_lpt_port)->list), 1);
	}
#endif // NOLPTLEDS

#ifndef NOKEYBOARDLEDS
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_keyboard_leds_on), cfg->keyboard_leds_on);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_keyboard_leds_inverted), cfg->keyboard_leds_invert);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_keyboard_order_inverted), cfg->keyboard_leds_order_invert);
	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_keyboard_scheme)->list), cfg->keyboard_leds_scheme);
#endif

#ifndef NOFLOPPYLEDS
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_floppy_leds_on), cfg->floppy_leds_on);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_floppy_leds_inverted), cfg->floppy_leds_invert);

	if (check_floppy_order_inverted)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_floppy_order_inverted), cfg->floppy_leds_order_invert);

	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_floppy_scheme)->list), cfg->floppy_leds_scheme);
#endif

	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo_general_filter)->list), cfg->general_filter);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_general_volume_dynamic), cfg->general_volume_dynamic);
	gtk_adjustment_set_value (GTK_ADJUSTMENT (adjustment_general_volume_max), cfg->general_volume_max);
	gtk_adjustment_set_value (GTK_ADJUSTMENT (adjustment_general_range_low), cfg->general_range_low);
	gtk_adjustment_set_value (GTK_ADJUSTMENT (adjustment_general_range_high), cfg->general_range_high);

	setting_defaults = FALSE;
}



void config_dialog (void)
{
	if (window_config)
	{
		if (GTK_WIDGET_VISIBLE (window_config))
			gdk_window_raise (window_config->window);

		gtk_notebook_set_page (GTK_NOTEBOOK (notebook), 1);
		return;
	}

	cfg = vis_get_configuration ();
	create_window_config ();
	config_dialog_set_defaults (cfg);
	gtk_notebook_set_page (GTK_NOTEBOOK (notebook), 1);
	gtk_widget_show (window_config);
}



void config_about (void)
{
	if (window_config)
	{
		if (GTK_WIDGET_VISIBLE (window_config))
			gdk_window_raise (window_config->window);

		gtk_notebook_set_page (GTK_NOTEBOOK (notebook), 0);
		return;
	}

	cfg = vis_get_configuration ();
	create_window_config ();
	config_dialog_set_defaults (cfg);
	gtk_notebook_set_page (GTK_NOTEBOOK (notebook), 0);
	gtk_widget_show (window_config);
}



void config_update (void)
{
	if (window_config)
	{
		cfg = vis_get_configuration ();
		config_dialog_set_defaults (cfg);
	}
}
