#### Header variables
%define name xmms-ledpower
%define version 1.0
%define release 1

# Override standard libdir with xmms config
%define _libdir /usr/lib/xmms/Visualization

#### Useful macros
# Attempt to avoid disaster
%define clean_build_root if test -O "%{buildroot}" -a "%{buildroot}" != "/" -a "%{buildroot}" != "$HOME"; then %{__rm} -rf %{buildroot}; fi
# To delete directory where compilation takes place
%define clean_build_dir %{__rm} -rf %{buildsubdir}

#### Header
Summary: XMMS - Visualization
Name: %{name}
Version: %{version}
Release: %{release}
Copyright: GPL
Group: Applications/Multimedia
Source: %{name}-%{version}.tar.gz
Prefix: %{_prefix}
Requires: xmms
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

#### Description
%description
Led Power uses leds connected to parallel port as well as keyboard leds
to visualize music played by XMMS.

#### Prep
%prep
%setup

#### Build
%build
%configure
# Build user might have his/her own special "make" hooks
%__make

#### Install
%install
%clean_build_root
%{!?makeinstall: %define makeinstall make DESTDIR="$RPM_BUILD_ROOT" install}
%makeinstall

#### Clean
%clean
%clean_build_root
%clean_build_dir

#### Post
# Nothing to do

#### Files
%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README
%{_libdir}/*
