/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  sound_processing.c
 */

#include "sound_processing.h"
#include "led_power.h"


static Volume vol;
static Configuration *cfg;
static int filter_in_use = 0;


static void filter_freq_scale (short data[2][256]);
static void filter_dynamic_freq_scale (short data[2][256]);

static void scheme_mono_vu (LedDevice *dev);
static void scheme_mono_vu_with_falloff (LedDevice *dev);
static void scheme_mono_split (LedDevice *dev);
static void scheme_stereo_split (LedDevice *dev);
static void scheme_beat_detect (LedDevice *dev);
static void scheme_relative_beat_level (LedDevice *dev);
static void scheme_progress (LedDevice *dev);
static void scheme_volume (LedDevice *dev);


FreqFilter filters[] =
{
	{"frequency scale", filter_freq_scale},
	{"dynamic frequency scale", filter_dynamic_freq_scale},
};

Scheme schemes[] =
{
	{"vu meter", scheme_mono_vu},
	{"vu meter with falloff", scheme_mono_vu_with_falloff},
	{"mono split", scheme_mono_split},
	{"stereo split", scheme_stereo_split},
	{"flash on beat", scheme_beat_detect},
	{"relative beat level", scheme_relative_beat_level},
	{"progress", scheme_progress},
	{"volume", scheme_volume},
};


void sp_init (Configuration * p)
{
	cfg = p;

	if (cfg->general_filter > ARRAY_SIZE (filters) || cfg->general_filter < 0)
		cfg->general_filter = 0;

	fft_prepare (FFT_NLOG2);
}


void sp_cleanup (void)
{
	fft_cleanup ();
}


int sp_get_filters (FreqFilter **p)
{
	if (p)
		*p = filters;

	return ARRAY_SIZE (filters);
}


int sp_get_schemes (Scheme **p)
{
	if (p)
		*p = schemes;

	return ARRAY_SIZE (schemes);
}


void sp_reset (void)
{
	memset (&vol, 0, sizeof (Volume));
}


void sp_process_pcm_data (short data[2][512], int filter)
{
	static short pass[2][256];
	static float out[2][512 * 2];
	int i, j;


	fft (&out[0][0], &data[0][0]);
	fft (&out[1][0], &data[1][0]);

	for (i = 0, j = 2; i < 256; i++, j += 2)
	{
		pass[0][i] = ((int) sqrt (out[0][j] * out[0][j] + out[0][j + 1] * out[0][j + 1])) >> 8;
		pass[1][i] = ((int) sqrt (out[1][j] * out[1][j] + out[1][j + 1] * out[1][j + 1])) >> 8;
	}

	vol.last_beat = vol.beat;
	vol.beat = 0;
	for (i = 1, j = 2; i < 4; i++, j += 2)
		vol.beat += sqrt ((out[0][j] * out[0][j] + out[0][j + 1] * out[0][j + 1] + out[1][j] * out[1][j] + out[1][j + 1] * out[1][j + 1]) / 2);

	vol.beat /= BEAT_MAGIC;

	sp_process_freq_data (pass, filter);
}


void sp_process_freq_data (short data[2][256], int filter)
{
	if (filter != filter_in_use)
	{
		sp_reset ();
		filter_in_use = filter;
	}

	filters[filter].process (data);
}


static void freq_calc_volume (Volume *v, short data[2][256])
{
	float l, r, b;
	int i, range;

	l = r = 0;


	for (i = cfg->general_range_low; i < cfg->general_range_high; i++)
	{
		l += i * data[0][i] >> 7;
		r += i * data[1][i] >> 7;
	}

	range = cfg->general_range_high - cfg->general_range_low;
	if (!range)
		range = 1;

	l /= range;
	r /= range;
	b = (l + r) / 2;

	v->left = l;
	v->right = r;
	v->both = b;
}


static void filter_freq_scale (short data[2][256])
{
	freq_calc_volume (&vol, data);

	if (cfg->general_volume_dynamic)
	{
		if (vol.left > vol.lmax || vol.right > vol.rmax || vol.both > vol.bmax)
		{
			vol.lmax = VMAX (vol.lmax, vol.left);
			vol.rmax = VMAX (vol.rmax, vol.right);
			vol.bmax = VMAX (vol.bmax, vol.both);
		}

		vol.left /= VMAX (vol.lmax, MIN_VOLUME_LEVEL);
		vol.right /= VMAX (vol.rmax, MIN_VOLUME_LEVEL);
		vol.both /= VMAX (vol.bmax, MIN_VOLUME_LEVEL);
	}
	else
	{
		vol.lmax = vol.rmax = vol.bmax = cfg->general_volume_max;

		vol.left /= VMAX (vol.lmax, 1);
		vol.right /= VMAX (vol.rmax, 1);
		vol.both /= VMAX (vol.bmax, 1);
	}

	vol.left = VMIN (vol.left, 1);
	vol.right = VMIN (vol.right, 1);
	vol.both = VMIN (vol.both, 1);
}


// acts the same as filter_freq_scale if dynamic volume is disabled
static void filter_dynamic_freq_scale (short data[2][256])
{
	int low_left, low_right, low_both;


	freq_calc_volume (&vol, data);

	if (cfg->general_volume_dynamic)
	{
		low_left = VMAX (vol.lmid, MIN_VOLUME_LEVEL);
		low_right = VMAX (vol.rmid, MIN_VOLUME_LEVEL);
		low_both = VMAX (vol.bmid, MIN_VOLUME_LEVEL);

		vol.lmid = (vol.lmid + vol.left) / 2;
		vol.rmid = (vol.rmid + vol.right) / 2;
		vol.bmid = (vol.bmid + vol.both) / 2;

		if (vol.left > vol.lmax || vol.right > vol.rmax || vol.both > vol.bmax)
		{
			vol.lmax = VMAX (vol.lmax, vol.left);
			vol.rmax = VMAX (vol.rmax, vol.right);
			vol.bmax = VMAX (vol.bmax, vol.both);
		}
		else
		{
			if (vol.lmax > low_left)
				vol.lmax -= 3;
			if (vol.rmax > low_right)
				vol.rmax -= 3;
			if (vol.bmax > low_both)
				vol.bmax -= 3;
		}

		vol.left /= VMAX (vol.lmax, MIN_VOLUME_LEVEL);
		vol.right /= VMAX (vol.rmax, MIN_VOLUME_LEVEL);
		vol.both /= VMAX (vol.bmax, MIN_VOLUME_LEVEL);
	}
	else
	{
		vol.lmax = vol.rmax = vol.bmax = cfg->general_volume_max;

		vol.left /= VMAX (vol.lmax, 1);
		vol.right /= VMAX (vol.rmax, 1);
		vol.both /= VMAX (vol.bmax, 1);
	}

	vol.left = VMIN (vol.left, 1);
	vol.right = VMIN (vol.right, 1);
	vol.both = VMIN (vol.both, 1);
}


static float set_falloff (LedDevice *dev, float t)
{
	if (t > dev->falloff)
		dev->falloff = t;
	else if (dev->falloff > 0)
		dev->falloff -= (SCHEME_FALLOFF_SPEED * dev->leds_count);

	return dev->falloff;
}


static void scheme_mono_vu (LedDevice *dev)
{
	int i, j;


	j = vol.both * dev->leds_count;
	for (i = 0; i < dev->leds_count; i++)
		if (i < j)
			dev->led_on (i);
		else
			dev->led_off (i);
}


static void scheme_mono_vu_with_falloff (LedDevice *dev)
{
	int i, j;


	j = (int) set_falloff (dev, vol.both * dev->leds_count);
	for (i = 0; i < dev->leds_count; i++)
		if (i < j)
			dev->led_on (i);
		else
			dev->led_off (i);
}


static void scheme_mono_split (LedDevice *dev)
{
	int i, b;
	int half_leds = dev->leds_count / 2;
	int odd = dev->leds_count & 1;


	half_leds += odd;

	b = vol.both * (dev->leds_count + odd) / 2;
	for (i = 0; i < half_leds; i++)
	{
		if (i < b)
		{
			dev->led_on ((half_leds - 1) - i);
			dev->led_on (half_leds + i);
		}
		else
		{
			dev->led_off ((half_leds - 1) - i);
			dev->led_off (half_leds + i);
		}
	}

	if (odd)
	{
		if (b > 1)
			dev->led_on (half_leds);
		else
			dev->led_off (half_leds);
	}
}


static void scheme_stereo_split (LedDevice *dev)
{
	int i, r, l;
	int half_leds = dev->leds_count / 2;
	int odd = dev->leds_count & 1;


	half_leds += odd;

	l = vol.left * (dev->leds_count + odd) / 2;
	for (i = 0; i < half_leds; i++)
		if (i < l)
			dev->led_on ((half_leds - 1) - i);
		else
			dev->led_off ((half_leds - 1) - i);

	r = vol.right * (dev->leds_count + odd) / 2;
	for (i = 0; i < half_leds; i++)
		if (i < r)
			dev->led_on (half_leds + i);
		else
			dev->led_off (half_leds + i);

	if (odd)
	{
		if (l > 1 || r > 1)
			dev->led_on (half_leds);
		else
			dev->led_off (half_leds);
	}
}


static void scheme_progress (LedDevice *dev)
{
	int i, j;
	float t;


	i = vis_get_song_time ();
	if (i == 0)
		i++;
	t = (float) vis_get_output_time () / i;

	j = (dev->leds_count + 1) * t;
	for (i = 0; i < dev->leds_count; i++)
		if (i < j)
			dev->led_on (i);
		else
			dev->led_off (i);
}


static void scheme_volume (LedDevice *dev)
{
	int i, j;
	float t;


	t = vis_get_volume ();

	j = (dev->leds_count + 1) * t;
	for (i = 0; i < dev->leds_count; i++)
		if (i < j)
			dev->led_on (i);
		else
			dev->led_off (i);
}


static void scheme_beat_detect (LedDevice *dev)
{
	if (vol.beat > 1.0 || vol.was_beat)
		dev->leds_all_on ();
	else
		dev->leds_all_off ();

	if (vol.was_beat)
	{
		if (dev->progress++ > BEAT_SUSTAIN)
		{
			vol.was_beat = 0;
			dev->progress = 0;
		}
	}
	else if (vol.beat > 1.0)
	{
		vol.was_beat = 1;
		dev->progress = 0;
	}

	return;
}


static void scheme_relative_beat_level (LedDevice *dev)
{
	int i, b;
	int half_leds = dev->leds_count / 2;
	int odd = dev->leds_count & 1;
	float beat = vol.beat - vol.last_beat;


	half_leds += odd;

	b = beat * (dev->leds_count + odd) / 2;
	for (i = 0; i < half_leds; i++)
	{
		if (i < b)
		{
			dev->led_on ((half_leds - 1) - i);
			dev->led_on (half_leds + i);
		}
		else
		{
			dev->led_off ((half_leds - 1) - i);
			dev->led_off (half_leds + i);
		}
	}

	if (odd)
	{
		if (b > 1)
			dev->led_on (half_leds);
		else
			dev->led_off (half_leds);
	}
}
