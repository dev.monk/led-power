/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  keyboard_leds.c
 */

#include "keyboard_leds.h"
#include "led_power.h"


static int keyboard_leds[] =
{
	LED_NUM_LOCK, LED_CAPS_LOCK, LED_SCROLL_LOCK
};

static int leds = 0;
static int keyboard = FALSE;
static int keyboard_opened = FALSE;
static int saved_state = 0;
static int configured = FALSE;

static CfgKeyboard cfg;
static sigset_t sigset;


int keyboard_open (void)
{
	if (keyboard_opened)
		return TRUE;

	if (!configured)
		keyboard_configure (NULL);

	keyboard = open ("/dev/console", O_RDWR);

	if (keyboard == -1)
	{
		keyboard_opened = FALSE;
		perror ("KEYBOARD_LEDS!: cannot open /dev/console");

		return FALSE;
	}
	else
		keyboard_opened = TRUE;

	if (KBD_REMEMBER_VALUE)
		ioctl (keyboard, KDGETLED, &saved_state);

	sigemptyset (&sigset);
	sigaddset (&sigset, SIGINT);
	sigaddset (&sigset, SIGTERM);
	sigaddset (&sigset, SIGQUIT);

	keyboard_leds_all_off ();
	keyboard_refresh ();

	return TRUE;
}


void keyboard_close (void)
{
	if (!keyboard_opened)
		return;

	if (KBD_REMEMBER_VALUE)
		ioctl (keyboard, KDSETLED, saved_state);
	close (keyboard);

	keyboard_opened = FALSE;
}


void keyboard_refresh (void)
{
	static int last_leds_state = -1;


	if (last_leds_state == leds)
		return;
	else
		last_leds_state = leds;

	if (keyboard_opened)
	{
		sigprocmask (SIG_BLOCK, &sigset, NULL);
		ioctl (keyboard, KDSETLED, leds);
		sigprocmask (SIG_UNBLOCK, &sigset, NULL);
	}
}


void keyboard_led_on (unsigned int led)
{
	int l;


	if (led >= KBD_LEDS_COUNT)
		return;
//
//  while (l < KBD_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	if (cfg.leds_invert)
		leds &= ~keyboard_leds[l];
	else
		leds |= keyboard_leds[l];
}


void keyboard_led_off (unsigned int led)
{
	int l;


	if (led >= KBD_LEDS_COUNT)
		return;
//
//	while (l < KBD_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	if (cfg.leds_invert)
		leds |= keyboard_leds[l];
	else
		leds &= ~keyboard_leds[l];
}


void keyboard_leds_all_on (void)
{
	if (cfg.leds_invert)
		leds = KBD_LED_NONE;
	else
		leds = KBD_LED_ALL;
}


void keyboard_leds_all_off (void)
{
	if (cfg.leds_invert)
		leds = KBD_LED_ALL;
	else
		leds = KBD_LED_NONE;
}


void keyboard_leds_invert (void)
{
	cfg.leds_invert = !cfg.leds_invert;
}


void keyboard_leds_order_invert (void)
{
	SWAP (cfg.leds_order[0], cfg.leds_order[2], int);
}


void keyboard_configure (Configuration * p)
{
	int i;


	configured = TRUE;

	if (p == NULL)
	{
		// setting defaults

		cfg.leds_on = TRUE;
		cfg.leds_invert = FALSE;

		for (i = 0; i < KBD_LEDS_COUNT; i++)
			cfg.leds_order[i] = i;
		cfg.leds_order[KBD_LEDS_COUNT] = -1;

		cfg.leds_order_invert = FALSE;
	}
	else
	{
		cfg.leds_invert = p->keyboard_leds_invert;
		cfg.leds_order_invert = p->keyboard_leds_order_invert;

		for (i = 0; i < KBD_LEDS_COUNT; i++)
			cfg.leds_order[i] = p->keyboard_leds_order[i];

		if (cfg.leds_order_invert)
			keyboard_leds_order_invert ();

		if (p->keyboard_leds_on)
		{
			cfg.leds_on = keyboard_open ();
		}
		else
		{
			keyboard_close ();
			cfg.leds_on = FALSE;
		}

                p->keyboard_leds_on = cfg.leds_on;
	}
}
