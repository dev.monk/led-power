#ifndef __LED_POWER_H
#define __LED_POWER_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <sys/io.h>

#ifndef USING_BMP
# include <plugin.h>
# include <xmmsctrl.h>
# include <util.h>
# include <configfile.h>
#else
# define ConfigFile ConfigDb
//# include <audacious/audctrl.h>
# include <audacious/plugin.h>
//# include <audacious/util.h>
# include <audacious/configdb.h>
//# include <audacious/dbus-service.h>
#endif


#include "configuration.h"
#include "sound_processing.h"


#define ARRAY_SIZE(a) 			(sizeof (a) / sizeof (*a))
#define VMAX(a, b) 					((a > b)? a : b)
#define VMIN(a, b) 					((a < b)? a : b)
#define SWAP(p1, p2, type) 	{ type t; t = p1; p1 = p2; p2 = t; }

#ifndef NOCONFIG
# include "config_dialog.h"
#endif
#ifndef NOIDLE
# include "idle.h"
#endif
#ifdef NOSCREENLEDS
# define SCREENLEDS 0
#else
# define SCREENLEDS 1
# include "screen_leds.h"
#endif
#ifdef NOLPTLEDS
# define LPTLEDS 0
#else
# define LPTLEDS 1
# include "lpt_leds.h"
#endif
#ifdef NOKEYBOARDLEDS
# define KEYBOARDLEDS 0
#else
# define KEYBOARDLEDS 1
# include "keyboard_leds.h"
#endif
#ifdef NOFLOPPYLEDS
# define FLOPPYLEDS 0
#else
# define FLOPPYLEDS 1
# include "floppy_leds.h"
#endif

#define LEDS_DEVICES		(SCREENLEDS + LPTLEDS + KEYBOARDLEDS + FLOPPYLEDS)
#define SCREENLEDS_N		0
#define LPTLEDS_N				(SCREENLEDS_N + SCREENLEDS)
#define KEYBOARDLEDS_N	(LPTLEDS_N + LPTLEDS)
#define FLOPPYLEDS_N		(KEYBOARDLEDS_N + KEYBOARDLEDS)

#define TIME_BEFORE_CLEANUP	6
#define TIME_BEFORE_IDLE		10

#define usleep xmms_usleep


void vis_disable (void);
void vis_configuration_changed (void);
Configuration *vis_get_configuration (void);
int vis_get_devices (LedDevice **p);
int vis_get_song_time (void);
int vis_get_output_time (void);
float vis_get_volume (void);



int cfg_read_boolean (ConfigFile *cf, char *section, char *key, int default_val);
int cfg_read_int (ConfigFile *cf, char *section, char *key, int default_val);
char *cfg_read_string (ConfigFile *cf, char *section, char *key, char *default_str);
void cfg_write_boolean (ConfigFile *cf, char *section, char *key, int val);
void cfg_write_int (ConfigFile *cf, char *section, char *key, int val);
void cfg_write_string (ConfigFile *cf, char *section, char *key, char *str);
ConfigFile *cfg_open_default_file (void);
void cfg_close (ConfigFile *cf);
void cfg_write (ConfigFile *cf);


#endif // __LED_POWER_H
