#ifndef __SOUND_PROCESSING_H
#define __SOUND_PROCESSING_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>

#include "configuration.h"
#include "fft.h"

#define SCHEME_FALLOFF_SPEED	0.025
#define BEAT_SUSTAIN					2

#define BEAT_MAGIC						3686400
#define MIN_VOLUME_LEVEL			100
#define FFT_NLOG2							9


typedef struct LedDeviceStruct
{
	char *name;
	unsigned int leds_count;

	int (*open) (void);
	void (*close) (void);
	void (*refresh) (void);
	void (*led_on) (unsigned int led);
	void (*led_off) (unsigned int led);
	void (*leds_all_on) (void);
	void (*leds_all_off) (void);
	void (*leds_invert) (void);
	void (*configure) (Configuration *p);

	// used by schemes
	float falloff;
	int progress;
} LedDevice;

typedef struct VolumeStruct
{
	float left;
	float lmid;
	float lmax;

	float right;
	float rmid;
	float rmax;

	float both;
	float bmid;
	float bmax;

	float beat;
	float last_beat;
	float was_beat;
} Volume;

typedef struct FreqFilterStruct
{
	char *name;
	void (*process) (short data[2][256]);
} FreqFilter;

typedef struct SchemeStruct
{
	char *name;
	void (*process) (LedDevice *dev);
} Scheme;


void sp_init (Configuration *p);
void sp_cleanup (void);
int sp_get_filters (FreqFilter **p);
int sp_get_schemes (Scheme **p);
void sp_reset (void);
void sp_process_freq_data (short data[2][256], int filter);
void sp_process_pcm_data (short data[2][512], int filter);


#endif // __SOUND_PROCESSING_H
