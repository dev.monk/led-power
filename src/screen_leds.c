/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  screen_leds.c
 */

#include "screen_leds.h"
#include "led_power.h"


static CfgScreen cfg;
static int configured = FALSE;
static int closing = FALSE;
static int switching_off = FALSE;
static int screen_leds[MAX_LEDS_COUNT];
static guint32 led_on_color[MAX_LEDS_COUNT];
static guint32 led_off_color[MAX_LEDS_COUNT];

static GtkWidget *win = NULL;
static GtkWidget *area = NULL;
static GdkGC *gc = NULL;
static GdkRgbCmap *cmap = NULL;

static guchar *buff_rgb = NULL;
static guint32 *buff_cmap = NULL;


static void window_create (void);
static void window_destroy (GtkWidget *widget, gpointer data);
static gboolean area_expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data);
static void rgb_draw_hline (guchar *buff, unsigned int y, unsigned int x1, unsigned int x2, unsigned int color);
static void rgb_draw_rect (guchar *buff, unsigned int x, unsigned int y, unsigned int a, unsigned int b, unsigned int color);
static void gradient_fill (guint32 *p, guint32 start_color, guint32 mid_color, guint32 end_color);


int screen_open (void)
{
	int i;


	if (win)
		return TRUE;

	buff_rgb = malloc (WIN_WIDTH * WIN_HEIGHT);
	buff_cmap = malloc (256 * sizeof (guint32));
	if (!(buff_rgb || buff_cmap))
	{
		printf ("SCREEN_LEDS: not enough memory for buffers!\n");
		screen_close ();		// clean up pointers

		return FALSE;
	}

	if (!configured)
		screen_configure (NULL);

	for (i = 0; i < SCR_LEDS_COUNT; i++)
		screen_leds[i] = LED_BASE_COLOR_NUM + i;

	gradient_fill (led_on_color, LED_ON_COLOR_START, LED_ON_COLOR_MID, LED_ON_COLOR_END);
	gradient_fill (led_off_color, LED_OFF_COLOR_START, LED_OFF_COLOR_MID, LED_OFF_COLOR_END);

	window_create ();

	screen_leds_all_off ();
	screen_refresh ();

	return (int) win;
}


void screen_close (void)
{
	if (gc)
	{
		gdk_gc_unref (gc);
		gc = NULL;
	}
	if (win)
	{
		closing = TRUE;
		gtk_widget_destroy (win);
		win = NULL;
	}

	if (buff_rgb)
	{
		free (buff_rgb);
		buff_rgb = NULL;
	}
	if (buff_cmap)
	{
		free (buff_cmap);
		buff_cmap = NULL;
	}
}


void screen_refresh (void)
{
	if (!win)
		return;

	cmap = gdk_rgb_cmap_new (buff_cmap, 256);

	gdk_draw_indexed_image (area->window, gc, 0, 0, WIN_WIDTH, WIN_HEIGHT, GDK_RGB_DITHER_NONE, buff_rgb, WIN_WIDTH, cmap);

	gdk_rgb_cmap_free (cmap);
}


void screen_led_on (unsigned int led)
{
	int l;


	if (led >= SCR_LEDS_COUNT || !buff_cmap)
		return;
//
//	while (l < SCR_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	l = screen_leds[l];

	if (cfg.leds_invert)
		buff_cmap[l] = led_off_color[l - LED_BASE_COLOR_NUM];
	else
		buff_cmap[l] = led_on_color[l - LED_BASE_COLOR_NUM];
}


void screen_led_off (unsigned int led)
{
	int l;


	if (led >= SCR_LEDS_COUNT || !buff_cmap)
		return;
//
//	while (l < SCR_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	l = screen_leds[l];

	if (cfg.leds_invert)
		buff_cmap[l] = led_on_color[l - LED_BASE_COLOR_NUM];
	else
		buff_cmap[l] = led_off_color[l - LED_BASE_COLOR_NUM];
}


void screen_leds_all_on (void)
{
	int i;


	for (i = 0; i < SCR_LEDS_COUNT; i++)
		screen_led_on (i);
}


void screen_leds_all_off (void)
{
	int i;


	for (i = 0; i < SCR_LEDS_COUNT; i++)
		screen_led_off (i);
}


void screen_leds_invert (void)
{
	cfg.leds_invert = !cfg.leds_invert;
}


void screen_leds_order_invert (void)
{
	int i, j;


	for (i = 0; i < SCR_LEDS_COUNT / 2; i++)
	{
		j = cfg.leds_order[i];
		cfg.leds_order[i] = cfg.leds_order[SCR_LEDS_COUNT - 1 - i];
		cfg.leds_order[SCR_LEDS_COUNT - 1 - i] = j;
	}
}


void screen_configure (Configuration *p)
{
	int i;


	configured = TRUE;

	if (p == NULL)
	{
		// setting defaults
		cfg.leds_on = TRUE;
		cfg.leds_invert = FALSE;

		for (i = 0; i < SCR_LEDS_COUNT; i++)
			cfg.leds_order[i] = i;
		cfg.leds_order[SCR_LEDS_COUNT] = -1;

		cfg.leds_order_invert = FALSE;
	}
	else
	{
		cfg.leds_invert = p->screen_leds_invert;
		cfg.leds_order_invert = p->screen_leds_order_invert;


		for (i = 0; i < MAX_LEDS_COUNT; i++)
			if (p->screen_leds_order[i] == -1)
				break;
		if (i != SCR_LEDS_COUNT)
		{
			// must reset leds_order

			for (i = 0; i < SCR_LEDS_COUNT; i++)
				p->screen_leds_order[i] = i;
			p->screen_leds_order[i] = -1;
		}

		for (i = 0; i < SCR_LEDS_COUNT; i++)
			cfg.leds_order[i] = p->screen_leds_order[i];

		if (cfg.leds_order_invert)
			screen_leds_order_invert ();

		if (p->screen_leds_on)
		{
			screen_open ();
			cfg.leds_on = TRUE;
			switching_off = FALSE;
		}
		else
		{
			// switching off
			switching_off = TRUE;
			screen_close ();
			cfg.leds_on = FALSE;
		}
	}
}


static gboolean area_expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
	screen_refresh ();

	return TRUE;
}


static void window_destroy (GtkWidget *widget, gpointer data)
{
	Configuration *c;


	cfg.leds_on = FALSE;
	win = NULL;

	if (!switching_off && !closing)
	{
		// closed by button
		screen_close ();
	}

	if (switching_off == closing)
	{
		// closed by button or by configure (screen_leds disabled)
		// notify config_dialog
		c = vis_get_configuration ();
		c->screen_leds_on = cfg.leds_on;
		vis_configuration_changed ();
	}

	closing = FALSE;
	switching_off = FALSE;
}


static void window_create (void)
{
	int i;
	float w, h;


	if (win)
		return;

	win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (win), WIN_TITLE);
	gtk_window_set_policy (GTK_WINDOW (win), FALSE, FALSE, FALSE);
	gtk_widget_set_usize (win, WIN_WIDTH, WIN_HEIGHT);
	gtk_widget_realize (win);

	gtk_signal_connect (GTK_OBJECT (win), "destroy", GTK_SIGNAL_FUNC (gtk_widget_destroyed), &win);
	gtk_signal_connect (GTK_OBJECT (win), "destroy", GTK_SIGNAL_FUNC (window_destroy), NULL);

	area = gtk_drawing_area_new ();
	gtk_container_add (GTK_CONTAINER (win), area);
	gtk_widget_realize (area);
	gc = gdk_gc_new (area->window);

	gtk_signal_connect (GTK_OBJECT (area), "expose_event", GTK_SIGNAL_FUNC (area_expose_event), NULL);

	gdk_rgb_init ();

	gdk_window_clear (area->window);
	gdk_window_clear (win->window);

	gtk_widget_show (area);
	gtk_widget_show (win);


	memset (buff_rgb, LED_BG_COLOR_NUM, WIN_WIDTH * WIN_HEIGHT);


	if (HORIZONTAL)
	{
		w = (float) (WIN_WIDTH - (2 * WIN_PADDING) - ((SCR_LEDS_COUNT - 1) * LED_PADDING)) / SCR_LEDS_COUNT;
		h = WIN_HEIGHT - (2 * WIN_PADDING);

		if (w > 0 && h > 0)
			for (i = 0; i < SCR_LEDS_COUNT; i++)
				rgb_draw_rect (buff_rgb, WIN_PADDING + (i * (w + LED_PADDING)), WIN_PADDING, (int) w, (int) h, i + LED_BASE_COLOR_NUM);
	}
	else
	{
		w = WIN_WIDTH - (2 * WIN_PADDING);
		h = (float) (WIN_HEIGHT - (2 * WIN_PADDING) - ((SCR_LEDS_COUNT - 1) * LED_PADDING)) / SCR_LEDS_COUNT;

		if (w > 0 && h > 0)
			for (i = 0; i < SCR_LEDS_COUNT; i++)
				rgb_draw_rect (buff_rgb, WIN_PADDING, WIN_PADDING + (i * (h + LED_PADDING)), (int) w, (int) h, i + LED_BASE_COLOR_NUM);
	}


	buff_cmap[LED_BG_COLOR_NUM] = LED_BG_COLOR;

	screen_leds_all_off ();
}


static void rgb_draw_hline (guchar *buff, unsigned int y, unsigned int x1, unsigned int x2, unsigned int color)
{
	int i;


	buff += y * WIN_WIDTH;

	if (x1 == x2)
		buff[x1] = color;
	else if (x1 < x2)
		for (i = x1; i < x2; i++)
			buff[i] = color;
	else
		for (i = x2; i < x1; i++)
			buff[i] = color;
}


static void rgb_draw_rect (guchar *buff, unsigned int x, unsigned int y, unsigned int a, unsigned int b, unsigned int color)
{
	int i;


	for (i = 0; i < b; i++)
		rgb_draw_hline (buff, y + i, x, x + a, color);
}


#if GTK_TYPE_GDK_BYTE_ORDER == GDK_LSB_FIRST
#define RED_SHIFT	16
#define RED_MASK	0x00ff0000
#define GREEN_SHIFT	8
#define GREEN_MASK	0x0000ff00
#define BLUE_SHIFT	0
#define BLUE_MASK	0x000000ff
#else
#define RED_SHIFT	0
#define RED_MASK	0x000000ff
#define GREEN_SHIFT	8
#define GREEN_MASK	0x0000ff00
#define BLUE_SHIFT	16
#define BLUE_MASK	0x00ff0000
#endif

static void gradient_fill (guint32 *p, guint32 start_color, guint32 mid_color, guint32 end_color)
{
	unsigned int first_red, last_red, first_green, last_green, first_blue, last_blue;
	unsigned int red, green, blue;
	int red_delta, green_delta, blue_delta;
	int spread = SCR_LEDS_COUNT / 2;
	int i, odd;


	odd = SCR_LEDS_COUNT & 1;

	first_red = (start_color & RED_MASK) >> RED_SHIFT;
	last_red = (mid_color & RED_MASK) >> RED_SHIFT;
	red_delta = last_red - first_red;
	first_green = (start_color & GREEN_MASK) >> GREEN_SHIFT;
	last_green = (mid_color & GREEN_MASK) >> GREEN_SHIFT;
	green_delta = last_green - first_green;
	first_blue = (start_color & BLUE_MASK) >> BLUE_SHIFT;
	last_blue = (mid_color & BLUE_MASK) >> BLUE_SHIFT;
	blue_delta = last_blue - first_blue;

	for (i = 0; i < spread; i++)
	{
		red = (first_red + (i * red_delta / spread)) & 0xff;
		green = (first_green + (i * green_delta / spread)) & 0xff;
		blue = (first_blue + (i * blue_delta / spread)) & 0xff;

		p[i] = (red << RED_SHIFT) | (green << GREEN_SHIFT) | (blue << BLUE_SHIFT);
	}

	first_red = (mid_color & RED_MASK) >> RED_SHIFT;
	last_red = (end_color & RED_MASK) >> RED_SHIFT;
	red_delta = last_red - first_red;
	first_green = (mid_color & GREEN_MASK) >> GREEN_SHIFT;
	last_green = (end_color & GREEN_MASK) >> GREEN_SHIFT;
	green_delta = last_green - first_green;
	first_blue = (mid_color & BLUE_MASK) >> BLUE_SHIFT;
	last_blue = (end_color & BLUE_MASK) >> BLUE_SHIFT;
	blue_delta = last_blue - first_blue;

	if (odd)
	{
		p[i] = mid_color;
		p++;
	}

	for (i = 0; i < spread; i++)
	{
		red = (first_red + ((i + 1) * red_delta / spread)) & 0xff;
		green = (first_green + ((i + 1) * green_delta / spread)) & 0xff;
		blue = (first_blue + ((i + 1) * blue_delta / spread)) & 0xff;

		p[i + spread] = (red << RED_SHIFT) | (green << GREEN_SHIFT) | (blue << BLUE_SHIFT);
	}
}
