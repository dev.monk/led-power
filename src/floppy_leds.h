#ifndef __FLOPPY_LEDS_H
#define __FLOPPY_LEDS_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "configuration.h"


#define PORT							0x03F2
#define LED_01						0x01
#define LED_02						0x02
#define DRIVE_A_LED_ON		(12 + 0 + (1 << (4 + 0)))
#define DRIVE_A_LED_OFF		(12 + 0)
#define DRIVE_B_LED_ON		(12 + 1 + (1 << (4 + 1)))
#define DRIVE_B_LED_OFF		(12 + 1)

// depends on how many drives are connected
#define FLOPPY_LEDS_COUNT	0x01
#define FLOPPY_LED_ALL		((FLOPPY_LEDS_COUNT == 2)? LED_01 : LED_01 | LED_02)
#define FLOPPY_LED_NONE		0x00
#define FLOPPY_REMEMBER_VALUE	1


typedef struct CfgFloppyStruct
{
	int leds_on;
	int leds_invert;
	int leds_order[FLOPPY_LEDS_COUNT];
	int leds_order_invert;
} CfgFloppy;


int floppy_open (void);
void floppy_close (void);
void floppy_refresh (void);
void floppy_led_on (unsigned int led);
void floppy_led_off (unsigned int led);
void floppy_leds_all_on (void);
void floppy_leds_all_off (void);
void floppy_leds_invert (void);
void floppy_leds_order_invert (void);
void floppy_configure (Configuration *p);


#endif // __FLOPPY_LEDS_H
