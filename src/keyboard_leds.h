#ifndef __KEYBOARD_LEDS_H
#define __KEYBOARD_LEDS_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/kd.h>
#include <sys/types.h>

#include "configuration.h"


#define LED_SCROLL_LOCK			LED_SCR
#define LED_NUM_LOCK				LED_NUM
#define LED_CAPS_LOCK				LED_CAP

#define KBD_LED_ALL					(LED_SCROLL_LOCK | LED_NUM_LOCK | LED_CAPS_LOCK)
#define KBD_LED_NONE				0x00
#define KBD_LEDS_COUNT			0x03
#define KBD_REMEMBER_VALUE	1


typedef struct CfgKeyboardStruct
{
	int leds_on;
	int leds_invert;
	int leds_order[KBD_LEDS_COUNT];
	int leds_order_invert;
} CfgKeyboard;


int keyboard_open (void);
void keyboard_close (void);
void keyboard_refresh (void);
void keyboard_led_on (unsigned int led);
void keyboard_led_off (unsigned int led);
void keyboard_leds_all_on (void);
void keyboard_leds_all_off (void);
void keyboard_leds_invert (void);
void keyboard_leds_order_invert (void);
void keyboard_configure (Configuration *p);


#endif // __KEYBOARD_LEDS_H
