/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  configuration.c
 */

#include "configuration.h"
#include "led_power.h"
#include "sound_processing.h"


static char *int_array_to_str (char *str, int *ints);
static int *str_to_int_array (int *ints, char *str);


void config_read (Configuration *cfg)
{
	ConfigFile *cf;
	LedDevice *devices;
	int dev_count, i;
	char *str;
	int schemes_count = sp_get_schemes (NULL);
	int filters_count = sp_get_filters (NULL);


	dev_count = vis_get_devices (&devices);
	cf = cfg_open_default_file ();
	if (cf)
	{
#ifndef NOSCREENLEDS
		cfg->screen_leds_on = cfg_read_boolean (cf, SECTION_NAME, "screen_leds_on", TRUE);
		cfg->screen_leds_invert = cfg_read_boolean (cf, SECTION_NAME, "screen_leds_invert", FALSE);
		cfg->screen_leds_order_invert = cfg_read_boolean (cf, SECTION_NAME, "screen_leds_order_invert", FALSE);
		cfg->screen_leds_scheme = cfg_read_int (cf, SECTION_NAME, "screen_leds_scheme", 0);
		if (cfg->screen_leds_scheme >= schemes_count || cfg->screen_leds_scheme < 0)
			cfg->screen_leds_scheme = 0;

		if ((str = cfg_read_string (cf, SECTION_NAME, "screen_leds_order", NULL)))
			str_to_int_array (cfg->screen_leds_order, str);
		else
		{
			for (i = 0; i < devices[SCREENLEDS_N].leds_count; i++)
				cfg->screen_leds_order[i] = i;
			cfg->screen_leds_order[i] = -1;
		}
		free (str);
#endif

#ifndef NOLPTLEDS
		cfg->lpt_leds_on = cfg_read_boolean (cf, SECTION_NAME, "lpt_leds_on", FALSE);
		cfg->lpt_leds_invert = cfg_read_boolean (cf, SECTION_NAME, "lpt_leds_invert", FALSE);
		cfg->lpt_leds_order_invert = cfg_read_boolean (cf, SECTION_NAME, "lpt_leds_order_invert", FALSE);
		if ((str = cfg_read_string (cf, SECTION_NAME, "lpt_port_address", "0x0378")))
			sscanf (str, "%x", &cfg->lpt_port_address);
		free (str);

		cfg->lpt_leds_scheme = cfg_read_int (cf, SECTION_NAME, "lpt_leds_scheme", 2);
		if (cfg->lpt_leds_scheme >= schemes_count || cfg->lpt_leds_scheme < 0)
			cfg->lpt_leds_scheme = 0;

		if ((str = cfg_read_string (cf, SECTION_NAME, "lpt_leds_order", NULL)))
			str_to_int_array (cfg->lpt_leds_order, str);
		else
		{
			for (i = 0; i < devices[LPTLEDS_N].leds_count; i++)
				cfg->lpt_leds_order[i] = i;
			cfg->lpt_leds_order[i] = -1;
		}
		free (str);
#endif

#ifndef NOKEYBOARDLEDS
		cfg->keyboard_leds_on = cfg_read_boolean (cf, SECTION_NAME, "keyboard_leds_on", TRUE);
		cfg->keyboard_leds_invert = cfg_read_boolean (cf, SECTION_NAME, "keyboard_leds_invert", FALSE);
		cfg->keyboard_leds_order_invert = cfg_read_boolean (cf, SECTION_NAME, "keyboard_leds_order_invert", FALSE);
		cfg->keyboard_leds_scheme = cfg_read_int (cf, SECTION_NAME, "keyboard_leds_scheme", 5);
		if (cfg->keyboard_leds_scheme >= schemes_count || cfg->keyboard_leds_scheme < 0)
			cfg->keyboard_leds_scheme = 0;

		if ((str = cfg_read_string (cf, SECTION_NAME, "keyboard_leds_order", NULL)))
			str_to_int_array (cfg->keyboard_leds_order, str);
		else
		{
			for (i = 0; i < devices[KEYBOARDLEDS_N].leds_count; i++)
				cfg->keyboard_leds_order[i] = i;
			cfg->keyboard_leds_order[i] = -1;
		}
		free (str);
#endif

#ifndef NOFLOPPYLEDS
		cfg->floppy_leds_on = cfg_read_boolean (cf, SECTION_NAME, "floppy_leds_on", FALSE);
		cfg->floppy_leds_invert = cfg_read_boolean (cf, SECTION_NAME, "floppy_leds_invert", FALSE);
		cfg->floppy_leds_order_invert = cfg_read_boolean (cf, SECTION_NAME, "floppy_leds_order_invert", FALSE);
		cfg->floppy_leds_scheme = cfg_read_int (cf, SECTION_NAME, "floppy_leds_scheme", 4);
		if (cfg->floppy_leds_scheme >= schemes_count || cfg->floppy_leds_scheme < 0)
			cfg->floppy_leds_scheme = 0;

		if ((str = cfg_read_string (cf, SECTION_NAME, "floppy_leds_order", NULL)))
			str_to_int_array (cfg->floppy_leds_order, str);
		else
		{
			for (i = 0; i < devices[FLOPPYLEDS_N].leds_count; i++)
				cfg->floppy_leds_order[i] = i;
			cfg->floppy_leds_order[i] = -1;
		}
		free (str);
#endif

		cfg->general_filter = cfg_read_int (cf, SECTION_NAME, "general_filter", 1);
		if (cfg->general_filter >= filters_count || cfg->general_filter < 0)
			cfg->general_filter = 0;

		cfg->general_volume_dynamic = cfg_read_boolean (cf, SECTION_NAME, "general_volume_dynamic", TRUE);
		cfg->general_volume_max = cfg_read_int (cf, SECTION_NAME, "general_volume_max", 110);
		cfg->general_range_low = cfg_read_int (cf, SECTION_NAME, "general_range_low", 0);
		cfg->general_range_high = cfg_read_int (cf, SECTION_NAME, "general_range_high", 180);
		if (cfg->general_range_low > cfg->general_range_high)
		{
			cfg->general_range_low = 0;
			cfg->general_range_high = 255;
		}

		cfg_close (cf);
	}
}


void config_write (Configuration * cfg)
{

	ConfigFile *cf;
	char str[MAX_LEDS_COUNT * 4];

	cf = cfg_open_default_file ();
	if (cf)
	{
#ifndef NOSCREENLEDS
		cfg_write_boolean (cf, SECTION_NAME, "screen_leds_on", cfg->screen_leds_on);
		cfg_write_boolean (cf, SECTION_NAME, "screen_leds_invert", cfg->screen_leds_invert);
		cfg_write_boolean (cf, SECTION_NAME, "screen_leds_order_invert", cfg->screen_leds_order_invert);
		cfg_write_int (cf, SECTION_NAME, "screen_leds_scheme", cfg->screen_leds_scheme);
		if (SCR_LEDS_COUNT <= 16)
		{
			int_array_to_str (str, cfg->screen_leds_order);
			cfg_write_string (cf, SECTION_NAME, "screen_leds_order", str);
		}
#endif

#ifndef NOLPTLEDS
		cfg_write_boolean (cf, SECTION_NAME, "lpt_leds_on", cfg->lpt_leds_on);
		cfg_write_boolean (cf, SECTION_NAME, "lpt_leds_invert", cfg->lpt_leds_invert);
		cfg_write_boolean (cf, SECTION_NAME, "lpt_leds_order_invert", cfg->lpt_leds_order_invert);
		cfg_write_int (cf, SECTION_NAME, "lpt_leds_scheme", cfg->lpt_leds_scheme);
		sprintf (str, "0x%.4x", cfg->lpt_port_address);
		cfg_write_string (cf, SECTION_NAME, "lpt_port_address", str);
		int_array_to_str (str, cfg->lpt_leds_order);
		cfg_write_string (cf, SECTION_NAME, "lpt_leds_order", str);
#endif

#ifndef NOKEYBOARDLEDS
		cfg_write_boolean (cf, SECTION_NAME, "keyboard_leds_on", cfg->keyboard_leds_on);
		cfg_write_boolean (cf, SECTION_NAME, "keyboard_leds_invert", cfg->keyboard_leds_invert);
		cfg_write_boolean (cf, SECTION_NAME, "keyboard_leds_order_invert", cfg->keyboard_leds_order_invert);
		cfg_write_int (cf, SECTION_NAME, "keyboard_leds_scheme", cfg->keyboard_leds_scheme);
		int_array_to_str (str, cfg->keyboard_leds_order);
		cfg_write_string (cf, SECTION_NAME, "keyboard_leds_order", str);
#endif

#ifndef NOKEYBOARDLEDS
		cfg_write_boolean (cf, SECTION_NAME, "floppy_leds_on", cfg->floppy_leds_on);
		cfg_write_boolean (cf, SECTION_NAME, "floppy_leds_invert", cfg->floppy_leds_invert);
		cfg_write_boolean (cf, SECTION_NAME, "floppy_leds_order_invert", cfg->floppy_leds_order_invert);
		cfg_write_int (cf, SECTION_NAME, "floppy_leds_scheme", cfg->floppy_leds_scheme);
		int_array_to_str (str, cfg->floppy_leds_order);
		cfg_write_string (cf, SECTION_NAME, "floppy_leds_order", str);
#endif

		cfg_write_int (cf, SECTION_NAME, "general_filter", cfg->general_filter);
		cfg_write_boolean (cf, SECTION_NAME, "general_volume_dynamic", cfg->general_volume_dynamic);
		cfg_write_int (cf, SECTION_NAME, "general_volume_max", cfg->general_volume_max);
		cfg_write_int (cf, SECTION_NAME, "general_range_low", cfg->general_range_low);
		cfg_write_int (cf, SECTION_NAME, "general_range_high", cfg->general_range_high);

		cfg_write (cf);
		cfg_close (cf);
	}
}


// closes array with -1
static int *str_to_int_array (int *ints, char *str)
{
	char temp[256];
	int result = 2, i = 0;


	strcpy (temp, str);
	while (result == 2)
		result = sscanf (temp, "%d %[0-9 ]", &ints[i++], temp);

	ints[i] = -1;

	return ints;
}


// 0 =< ints < 999, -1 closes array
static char *int_array_to_str (char *str, int *ints)
{
	int i = 0;


	*str = '\0';				// clear the string

	while (ints[i] != -1)
		sprintf (str, "%s%d ", str, ints[i++]);

	return str;
}
