/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  lpt_leds.c
 */

#include "lpt_leds.h"
#include "led_power.h"


static int lpt_leds[] =
{
	LED_01, LED_02, LED_03, LED_04, LED_05, LED_06, LED_07, LED_08
};

static int leds = 0;
static int opened = FALSE;
static int saved_state = 0;
static int configured = FALSE;

static CfgLpt cfg;


int lpt_open (void)
{
	if (opened)
		return TRUE;

	if (!configured)
		lpt_configure (NULL);

	// 'ioperm' returns 0 on success and -1 on error
	opened = ioperm (cfg.port, 3, TRUE) + 1;

	if (!opened)
	{
		perror ("LPT_LEDS!: cannot open parallel port");

		return FALSE;
	}

	if (LPT_REMEMBER_VALUE)
		saved_state = inb (cfg.port);

	lpt_leds_all_off ();
	lpt_refresh ();

	return TRUE;
}


void lpt_close (void)
{
	if (!opened)
		return;

	if (LPT_REMEMBER_VALUE)
		outb (saved_state, cfg.port);
	ioperm (cfg.port, 3, FALSE);

	opened = FALSE;
}


void lpt_refresh (void)
{
	static int last_leds_state = -1;


	if (last_leds_state == leds)
		return;
	else
		last_leds_state = leds;

	if (opened)
		outb (leds, cfg.port);
}


void lpt_led_on (unsigned int led)
{
	int l;


	if (led >= LPT_LEDS_COUNT)
		return;
//
//	while (l < LPT_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	if (cfg.leds_invert)
		leds &= ~lpt_leds[l];
	else
		leds |= lpt_leds[l];
}


void lpt_led_off (unsigned int led)
{
	int l;


	if (led >= LPT_LEDS_COUNT)
		return;
//
//	while (l < LPT_LEDS_COUNT && cfg.leds_order[l] != led)
//		l++;
//
	l = cfg.leds_order[led];
//
	if (cfg.leds_invert)
		leds |= lpt_leds[l];
	else
		leds &= ~lpt_leds[l];
}


void lpt_leds_all_on (void)
{
	if (cfg.leds_invert)
		leds = LPT_LED_NONE;
	else
		leds = LPT_LED_ALL;
}


void lpt_leds_all_off (void)
{
	if (cfg.leds_invert)
		leds = LPT_LED_ALL;
	else
		leds = LPT_LED_NONE;
}


void lpt_leds_invert (void)
{
	cfg.leds_invert = !cfg.leds_invert;
}


void lpt_leds_order_invert (void)
{
	int i, j;


	for (i = 0; i < LPT_LEDS_COUNT / 2; i++)
	{
		j = cfg.leds_order[i];
		cfg.leds_order[i] = cfg.leds_order[LPT_LEDS_COUNT - 1 - i];
		cfg.leds_order[LPT_LEDS_COUNT - 1 - i] = j;
	}
}


void lpt_configure (Configuration *p)
{
	int i;


	configured = TRUE;

	if (p == NULL)
	{
		// setting defaults

		cfg.leds_on = TRUE;
		cfg.port = DEFAULT_PORT_ADDRESS;
		cfg.leds_invert = FALSE;

		for (i = 0; i < LPT_LEDS_COUNT; i++)
			cfg.leds_order[i] = i;
		cfg.leds_order[LPT_LEDS_COUNT] = -1;

		cfg.leds_order_invert = FALSE;
	}
	else
	{
		cfg.leds_invert = p->lpt_leds_invert;
		cfg.leds_order_invert = p->lpt_leds_order_invert;

		for (i = 0; i < LPT_LEDS_COUNT; i++)
			cfg.leds_order[i] = p->lpt_leds_order[i];

		if (cfg.leds_order_invert)
			lpt_leds_order_invert ();

		if (p->lpt_port_address != 0x0278 &&
				p->lpt_port_address != 0x0378 &&
				p->lpt_port_address != 0x02BC &&
				p->lpt_port_address != 0x03BC)
		{
			printf ("LPT_LEDS!: invalid port address, using default\n");

			p->lpt_port_address = DEFAULT_PORT_ADDRESS;
		}

		if (cfg.port != p->lpt_port_address)
		{
			if (cfg.leds_on)
			{
				lpt_close ();
				cfg.port = p->lpt_port_address;
				cfg.leds_on = lpt_open ();
			}
			else
				cfg.port = p->lpt_port_address;
		}

		if (p->lpt_leds_on)
		{
			cfg.leds_on = lpt_open ();
		}
		else
		{
			lpt_close ();
			cfg.leds_on = FALSE;
		}

                p->lpt_leds_on = cfg.leds_on;
	}
}
