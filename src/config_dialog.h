#ifndef __CONFIG_DIALOG_H
#define __CONFIG_DIALOG_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "configuration.h"


void config_dialog (void);
void config_about (void);
void config_update (void);


#endif // __CONFIG_DIALOG_H
