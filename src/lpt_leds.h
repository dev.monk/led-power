#ifndef __LPT_LEDS_H
#define __LPT_LEDS_H 1


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "configuration.h"


#define DEFAULT_PORT_ADDRESS	0x0378
#define LED_01								0x01
#define LED_02								0x02
#define LED_03								0x04
#define LED_04								0x08
#define LED_05								0x10
#define LED_06								0x20
#define LED_07								0x40
#define LED_08								0x80

#define LPT_LED_ALL						0xff
#define LPT_LED_NONE					0x00
#define LPT_LEDS_COUNT				0x08
#define LPT_REMEMBER_VALUE		1


typedef struct CfgLptStruct
{
	int port;
	int leds_on;
	int leds_invert;
	int leds_order[LPT_LEDS_COUNT];
	int leds_order_invert;
} CfgLpt;


int lpt_open (void);
void lpt_close (void);
void lpt_refresh (void);
void lpt_led_on (unsigned int led);
void lpt_led_off (unsigned int led);
void lpt_leds_all_on (void);
void lpt_leds_all_off (void);
void lpt_leds_invert (void);
void lpt_leds_order_invert (void);
void lpt_configure (Configuration *p);


#endif // __LPT_LEDS_H
