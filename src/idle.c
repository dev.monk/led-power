/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 *
 *
 *  mock
 *  idle.c
 */

#include "idle.h"
#include "led_power.h"


static gint idle_id = 0;
static GTimer *gt = NULL;
static LedDevice *devices = NULL;
static int devices_count = 0;
static unsigned int wait_before_cleanup = 0;
static unsigned int wait_before_idle = 0;
static int cleanup_leds = FALSE;
static int cleanup_scheme_n = 0;
static int idle_scheme_n = 0;


static gint idle_function (gpointer data);

static int idle_cleanup_scheme_quick (LedDevice *dev);
static int idle_cleanup_scheme_slow (LedDevice *dev);
static int idle_cleanup_scheme_slow_reversed (LedDevice *dev);
static int idle_cleanup_scheme_slow_double (LedDevice *dev);
static int idle_cleanup_scheme_random_erase (LedDevice *dev);

#ifdef ENABLE_FADE_TRICK
static int idle_cleanup_scheme_fade_trick (LedDevice *dev);
#endif

static void idle_scheme_flash (LedDevice *dev);
static void idle_scheme_flash_odd_even (LedDevice *dev);
static void idle_scheme_simple (LedDevice *dev);
static void idle_scheme_simple_reversed (LedDevice *dev);
static void idle_scheme_simple_double (LedDevice *dev);
static void idle_scheme_random (LedDevice *dev);
static void idle_scheme_ball (LedDevice *dev);
static void idle_scheme_cosine (LedDevice *dev);
static void idle_scheme_cosine_reversed (LedDevice *dev);


IdleCleanupScheme cleanup_schemes[] =
{
	{"quick", idle_cleanup_scheme_quick},
	{"slow", idle_cleanup_scheme_slow},
	{"slow reversed", idle_cleanup_scheme_slow_reversed},
	{"double slow", idle_cleanup_scheme_slow_double},
	{"random erase", idle_cleanup_scheme_random_erase},
#ifdef ENABLE_FADE_TRICK
	{"fade trick", idle_cleanup_scheme_fade_trick},
#endif
};

IdleScheme idle_schemes[] =
{
	{"flash", idle_scheme_flash},
	{"odd/even flash", idle_scheme_flash_odd_even},
	{"simple", idle_scheme_simple},
	{"simple reversed", idle_scheme_simple_reversed},
	{"double simple", idle_scheme_simple_double},
	{"random", idle_scheme_random},
	{"ball", idle_scheme_ball},
	{"cosine", idle_scheme_cosine},
	{"cosine reversed", idle_scheme_cosine_reversed},
};


void idle_enter (void)
{
	int i, cleanup_schemes_count, idle_schemes_count;


	cleanup_leds = TRUE;

	devices_count = vis_get_devices (&devices);

	cleanup_schemes_count = ARRAY_SIZE (cleanup_schemes);
	idle_schemes_count = ARRAY_SIZE (idle_schemes);

	for (i = 0; i < devices_count; i++)
		devices[i].progress = 0;

	srandom (time (NULL));
	cleanup_scheme_n = cleanup_schemes_count * ((float) random () / RAND_MAX);
	idle_scheme_n = idle_schemes_count * ((float) random () / RAND_MAX);

	gt = g_timer_new ();
	g_timer_start (gt);

	idle_id = gtk_timeout_add (TIMEOUT_INTERVAL, idle_function, NULL);
}


void idle_enter_after_time (unsigned int time_before_cleanup, unsigned int time_before_idle)
{
	wait_before_cleanup = time_before_cleanup;
	wait_before_idle = time_before_idle;

	idle_enter ();
}


void idle_exit (void)
{
	g_timer_destroy (gt);
	gtk_timeout_remove (idle_id);

	wait_before_cleanup = 0;
	wait_before_idle = 0;
}


static gint idle_function (gpointer data)
{
	static gdouble elapsed = 0;
	static gdouble time_after_cleanup = 0;
	int i;


	if (wait_before_cleanup)
	{
		elapsed = g_timer_elapsed (gt, NULL);
		if (elapsed >= wait_before_cleanup)
		{
			g_timer_reset (gt);
			wait_before_cleanup = 0;
		}

		return TRUE;
	}

	if (cleanup_leds)
	{
		int status = 0;

		for (i = 0; i < devices_count; i++)
			status += cleanup_schemes[cleanup_scheme_n].process (&devices[i]);

		if (status == devices_count)
		{
			cleanup_leds = FALSE;

			for (i = 0; i < devices_count; i++)
				devices[i].progress = 0;
		}

		time_after_cleanup = g_timer_elapsed (gt, NULL);

		return TRUE;
	}

	if (wait_before_idle)
	{
		elapsed = g_timer_elapsed (gt, NULL);
		if (elapsed - time_after_cleanup >= wait_before_idle)
		{
			g_timer_stop (gt);
			wait_before_idle = 0;
		}

		return TRUE;
	}

	for (i = 0; i < devices_count; i++)
		idle_schemes[idle_scheme_n].process (&devices[i]);


	return TRUE;
}


static int idle_cleanup_scheme_quick (LedDevice *dev)
{
	dev->leds_all_off ();
	dev->refresh ();

	return TRUE;
}


static int idle_cleanup_scheme_slow (LedDevice *dev)
{
	int i;


	i = (float) dev->progress * dev->leds_count / MAX_LEDS_COUNT;

	if (i > 0)
		dev->led_off (i - 1);

	dev->led_on (i);
	dev->refresh ();

	if (dev->progress++ == MAX_LEDS_COUNT)
		return TRUE;
	else
		return FALSE;
}


static int idle_cleanup_scheme_slow_reversed (LedDevice *dev)
{
	int i;


	i = (float) dev->progress * dev->leds_count / MAX_LEDS_COUNT;

	if (i > 0)
	{
		dev->led_off (i - 1);
		dev->led_off (dev->leds_count - 1 - (i - 1));
	}

	dev->led_on (i);
	dev->led_on (dev->leds_count - 1 - i);

	dev->refresh ();

	if (dev->progress++ == MAX_LEDS_COUNT)
		return TRUE;
	else
		return FALSE;
}


static int idle_cleanup_scheme_slow_double (LedDevice *dev)
{
	int i;


	i = (float) dev->progress * dev->leds_count / MAX_LEDS_COUNT;

	if (i > 0)
		dev->led_off (dev->leds_count - 1 - (i - 1));

	dev->led_on (dev->leds_count - 1 - i);
	dev->refresh ();

	if (dev->progress++ == MAX_LEDS_COUNT)
		return TRUE;
	else
		return FALSE;
}


static int idle_cleanup_scheme_random_erase (LedDevice *dev)
{
	static int prepared = FALSE;
	static int order[MAX_LEDS_COUNT];
	int i;


	if (!prepared)
	{
		int order_used[MAX_LEDS_COUNT];
		int t = 0;

		for (i = 0; i < MAX_LEDS_COUNT; i++)
			order_used[i] = FALSE;

		for (i = 0; i < MAX_LEDS_COUNT; i++)
		{
			do
				t = MAX_LEDS_COUNT * ((float) random () / RAND_MAX);
			while (order_used[t]);

			order[i] = t;
			order_used[t] = TRUE;
		}

		prepared = TRUE;
	}

	dev->led_off (order[dev->progress]);
	dev->refresh ();

	if (++dev->progress == MAX_LEDS_COUNT)
		return TRUE;
	else
		return FALSE;
}


#ifdef ENABLE_FADE_TRICK
// realtime priority must be enabled
static int idle_cleanup_scheme_fade_trick (LedDevice *dev)
{
	int i;


	// works only with lpt and floppy leds
	if (strcmp (dev->name, "lpt_leds") != 0 &&
			strcmp (dev->name, "floppy_leds") != 0)
	{
		idle_cleanup_scheme_quick (dev);
		return TRUE;
	}

	dev->leds_all_on ();

	for (i = 0; i < FADE_TRICK_LOOP; i++)
	{
		dev->leds_all_on ();
		dev->refresh ();
		usleep (FADE_TRICK_LOOP - i);

		dev->leds_all_off ();
		dev->refresh ();
		usleep (i);
	}

	return TRUE;
}
#endif


static void idle_scheme_flash (LedDevice *dev)
{
	if (dev->progress == FLASH_DELAY)
		dev->leds_all_on ();
	else if (dev->progress == 2 * FLASH_DELAY)
	{
		dev->leds_all_off ();
		dev->progress = 0;
	}

	dev->progress++;
	dev->refresh ();
}


static void idle_scheme_flash_odd_even (LedDevice *dev)
{
	int i;


	if (dev->progress == FLASH_DELAY)
	{
		for (i = 0; i < dev->leds_count; i++)
		{
			if (i & 1)
				dev->led_on (i);
			else
				dev->led_off (i);
		}
	}
	else if (dev->progress == 2 * FLASH_DELAY)
	{
		for (i = 0; i < dev->leds_count; i++)
		{
			if (i & 1)
				dev->led_off (i);
			else
				dev->led_on (i);
		}

		dev->progress = 0;
	}

	dev->progress++;
	dev->refresh ();
}


static void idle_scheme_simple (LedDevice *dev)
{
	int i;


	i = (float) dev->leds_count * dev->progress / MAX_LEDS_COUNT;

	dev->leds_all_off ();
	if (dev->progress < MAX_LEDS_COUNT)
		dev->led_on (i);
	else if (dev->progress > MAX_LEDS_COUNT + 2)
		dev->progress = -1;

	dev->progress++;
	dev->refresh ();
}


static void idle_scheme_simple_reversed (LedDevice *dev)
{
	int i;


	i = (float) dev->leds_count * dev->progress / MAX_LEDS_COUNT;

	dev->leds_all_off ();
	if (dev->progress < MAX_LEDS_COUNT)
		dev->led_on (dev->leds_count - 1 - i);
	else if (dev->progress > MAX_LEDS_COUNT + 2)
		dev->progress = -1;

	dev->progress++;
	dev->refresh ();
}


static void idle_scheme_simple_double (LedDevice *dev)
{
	int i;


	if (dev->leds_count < 2)
		return idle_scheme_simple (dev);

	i = (float) dev->leds_count * dev->progress / MAX_LEDS_COUNT;

	dev->leds_all_off ();
	if (dev->progress < MAX_LEDS_COUNT)
	{
		dev->led_on (i);
		dev->led_on (dev->leds_count - 1 - i);
	}
	else if (dev->progress > MAX_LEDS_COUNT + 2)
		dev->progress = -1;

	dev->progress++;
	dev->refresh ();
}


static void idle_scheme_random (LedDevice *dev)
{
	static int prepared = FALSE;
	static int order[MAX_LEDS_COUNT * 4];
	int i;


	if (!prepared)
	{
		int order_used[MAX_LEDS_COUNT * 4];
		int t = 0;

		for (i = 0; i < MAX_LEDS_COUNT * 4; i++)
			order_used[i] = FALSE;

		for (i = 0; i < MAX_LEDS_COUNT * 4; i++)
		{
			do
				t = MAX_LEDS_COUNT * 4 * ((float) random () / RAND_MAX);
			while (order_used[t]);
			order_used[t] = TRUE;
			order[i] = t;
		}

		prepared = TRUE;
	}

	dev->leds_all_off ();
	dev->led_on (order[dev->progress]);
	dev->refresh ();

	if (++dev->progress == MAX_LEDS_COUNT * 4)
		dev->progress = 0;
}


static void idle_scheme_ball (LedDevice *dev)
{
	int i;


	if (dev->leds_count < 2)
		return idle_scheme_random (dev);

	if (dev->progress == MAX_LEDS_COUNT)
		dev->progress = 0;

	i = (float) 2 * (dev->leds_count - 1) * dev->progress / MAX_LEDS_COUNT;

	if (i > (dev->leds_count - 1))
		i = 2 * (dev->leds_count - 1) - i;

	dev->progress++;

	dev->leds_all_off ();
	dev->led_on (i);
	dev->refresh ();
}


static void idle_scheme_cosine (LedDevice *dev)
{
	int i;


	if (dev->leds_count < 2)
		return idle_scheme_random (dev);

	i = ((float) dev->leds_count - 0.5) * cos (dev->progress * M_PI / 180);

	if (dev->progress == 360)
		dev->progress = 0;
	else
		dev->progress += 2;

	dev->leds_all_off ();
	dev->led_on (abs (i));
	dev->refresh ();
}


static void idle_scheme_cosine_reversed (LedDevice *dev)
{
	int i;


	if (dev->leds_count < 2)
		return idle_scheme_random (dev);

	i = ((float) dev->leds_count - 0.5) * cos (dev->progress * M_PI / 180);

	if (dev->progress == 360)
		dev->progress = 0;
	else
		dev->progress++;

	dev->leds_all_off ();
	dev->led_on (dev->leds_count - 1 - abs (i));
	dev->refresh ();
}
